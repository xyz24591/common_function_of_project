package com.neo.androidcommon.Entity;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by Administrator on 2018/5/3.
 */

public class DemoDetails {

    private int titleId;
    private int descriptionId;
    private Class<? extends AppCompatActivity> activityClass;

    public DemoDetails(int titleId, int descriptionId, Class<? extends AppCompatActivity> activityClass) {
        this.titleId = titleId;
        this.descriptionId = descriptionId;
        this.activityClass = activityClass;
    }

    public int getTitleId() {
        return titleId;
    }

    public void setTitleId(int titleId) {
        this.titleId = titleId;
    }

    public int getDescriptionId() {
        return descriptionId;
    }

    public void setDescriptionId(int descriptionId) {
        this.descriptionId = descriptionId;
    }

    public Class<? extends AppCompatActivity> getActivityClass() {
        return activityClass;
    }

    public void setActivityClass(Class<? extends AppCompatActivity> activityClass) {
        this.activityClass = activityClass;
    }
}
