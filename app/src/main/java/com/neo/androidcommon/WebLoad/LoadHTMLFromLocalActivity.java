package com.neo.androidcommon.WebLoad;

import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import butterknife.BindView;

/**
 * Created by Administrator on 2018/5/22.
 */

public class LoadHTMLFromLocalActivity extends BaseActivity {
    @BindView(R.id.web_html)
    WebView webHtml;

    @Override
    public int intiLayout() {
        return R.layout.load_html_layout;
    }

    @Override
    public void initView() {

        WebSettings settings = webHtml.getSettings();

        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        webHtml.loadUrl("file:///android_asset/index.html");
        webHtml.setWebChromeClient(new WebChromeClient());
    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }
}
