package com.neo.androidcommon.MaterialDesignDemo;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/10/17.
 */

public class MDMainactivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.md_snack_btn)
    Button mdSnackBtn;
    @BindView(R.id.md_root)
    LinearLayout mdRoot;

    @BindView(R.id.tl_username)
    TextInputLayout tlUserName;
    @BindView(R.id.et_username)
    EditText etUserName;
    @BindView(R.id.tl_password)
    TextInputLayout tlPassword;
    @BindView(R.id.et_password)
    EditText etPassword;

    @BindView(R.id.md_login_btn)
    Button mdLoginBtn;

    private static final String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";

    @Override
    public int intiLayout() {
        return R.layout.activity_md;
    }

    @Override
    public void initView() {

        mdSnackBtn.setOnClickListener(this);
        mdLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                login();
            }
        });

    }

    private void login() {

        String username = tlUserName.getEditText().getText().toString();
        String password = tlPassword.getEditText().getText().toString();

        if (!validateUserName(username)) {

            tlUserName.setErrorEnabled(true);
            tlUserName.setError("请输入正确的 邮箱地址");

        } else if (!validatePassword(password)) {
            tlPassword.setErrorEnabled(true);
            tlPassword.setError("密码字数过少");

        } else {
            tlUserName.setErrorEnabled(false);
            tlPassword.setErrorEnabled(false);
            Toast.makeText(this, "登录成功", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }


    @Override
    public void onClick(View v) {

        showSnackBar();

    }

    private void showSnackBar() {

        Snackbar.make(mdRoot, "我出来了", Snackbar.LENGTH_LONG)
                .setAction("点击事件", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MDMainactivity.this, "点我...", Toast.LENGTH_SHORT).show();
                    }
                })
                .setDuration(Snackbar.LENGTH_LONG)
                .show();


    }


    private boolean validatePassword(String password) {

        return password.length() > 6;

    }

    private boolean validateUserName(String userName) {

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(userName);


        return matcher.matches();

    }


}
