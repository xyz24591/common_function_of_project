package com.neo.androidcommon.MaterialDesignDemo;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.MaterialDesignDemo.adapter.RecyclerViewAdapter;
import com.neo.androidcommon.R;

/**
 * Created by Administrator on 2018/10/18.
 */

public class MDCoorDetailActivity extends BaseActivity {
    @Override
    public int intiLayout() {
        return R.layout.activity_detail;
    }

    @Override
    public void initView() {
        Toolbar toolbar = (Toolbar) this.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("哆啦A梦");
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);



        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(new RecyclerViewAdapter(this));

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }
}
