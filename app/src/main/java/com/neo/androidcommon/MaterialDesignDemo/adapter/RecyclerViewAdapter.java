package com.neo.androidcommon.MaterialDesignDemo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.neo.androidcommon.MaterialDesignDemo.MDCoorActivity;
import com.neo.androidcommon.MaterialDesignDemo.MDCoorDetailActivity;
import com.neo.androidcommon.R;

/**
 * Created by Administrator on 2018/10/18.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private Context context;

    public RecyclerViewAdapter(Context context) {
        this.context = context;
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.list_item_card_main, parent, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final View view = holder.mView;
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "奔跑在孤傲的路上", Toast.LENGTH_SHORT).show();

//                Intent mIntent = new Intent(context, MDCoorActivity.class);
                Intent mIntent = new Intent(context, MDCoorDetailActivity.class);
                context.startActivity(mIntent);

            }
        });
    }


    @Override
    public int getItemCount() {
        return 10;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
        }
    }

}
