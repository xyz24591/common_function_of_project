package com.neo.androidcommon.sqlitedb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Administrator on 2018/9/7.
 */

public class MyDBHelper extends SQLiteOpenHelper {
    public MyDBHelper(Context context) {
        super(context, "my.db", null, 1);
    }

//   首次 创建数据库 时候 调用

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE person(personid INTEGER PRIMARY KEY AUTOINCREMENT,name VARCHAR(20))");

    }


    //    软件 版本号 发生 改变时候 调用
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("ALERT TABLE person ADD phone VARCHAR(12) NULL");

        switch (oldVersion) {

            case 1:
                db.execSQL("第一个版本的建表语句");
            case 2:
                db.execSQL("第二个版本的建表语句");
            case 3:
                db.execSQL("第三个版本的建表语句");
            default:
                break;
        }


//        旧表的设计太糟糕，很多字段要改，改动太多，想建一个新表，但是表名要一样 而且以前的一些数据要保存到新表中！
//
//        答：呵呵，给你跪了，当然，也有解决办法，下面说下思路：
//
//        1.将旧表改名成临时表: ALTER TABLE User RENAME TO _temp_User;
//
//        2.创建新表: CREATE TABLE User (u_id INTEGER PRIMARY KEY,u_name VARCHAR(20),u_age VARCHAR(4));
//
//        3.导入数据； INSERT INTO User SELECT u_id,u_name,"18" FROM _temp_User; //原表中没有的要自己设个默认值
//
//        4.删除临时表； DROP TABLE_temp_User;

    }
}
