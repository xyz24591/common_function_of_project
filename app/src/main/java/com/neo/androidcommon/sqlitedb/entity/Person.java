package com.neo.androidcommon.sqlitedb.entity;

/**
 * Created by Administrator on 2018/9/10.
 */

public class Person {

    private String name;
    private String phone;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
