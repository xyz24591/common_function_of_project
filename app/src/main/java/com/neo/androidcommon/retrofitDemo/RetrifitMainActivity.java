package com.neo.androidcommon.retrofitDemo;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;
import com.neo.androidcommon.RecyclerViewFreshLoadMore.SwipeRecyclerView;
import com.neo.androidcommon.retrofitDemo.constant.Constant;
import com.neo.androidcommon.retrofitDemo.entity.PostInfo;
import com.neo.androidcommon.retrofitDemo.impl.RetrofitService;
import com.neo.androidcommon.retrofitDemo.manger.SendMessageManager;
import com.neo.androidcommon.retrofitDemo.net.RetrofitUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Administrator on 2018/10/16.
 */

public class RetrifitMainActivity extends BaseActivity {
    @BindView(R.id.btn_base_request)
    Button btnBaseRequest;
    @BindView(R.id.btn_rx_request)
    Button btnRxRequest;
    @BindView(R.id.btn_encap_request)
    Button btnEncapRequest;

    @Override
    public int intiLayout() {
        return R.layout.activity_retrofit_demo;
    }

    @Override
    public void initView() {


    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }


    @OnClick({R.id.btn_base_request, R.id.btn_rx_request, R.id.btn_encap_request})
    public void OnClick(View view) {

        switch (view.getId()) {

            case R.id.btn_base_request:

                baseRequest();
                break;
            case R.id.btn_rx_request:
// Rx方式使用
                rxRequest();
                break;
            case R.id.btn_encap_request:

                // 封装使用
                encapRequest();

                break;

            default:
                break;
        }

    }

    private SendMessageManager sendMessageManager;

    private void encapRequest() {

        if (sendMessageManager == null) {
            sendMessageManager = SendMessageManager.getInstance();

        }

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }


        sendMessageManager.getPostInfo("yuantong", "11111111111");

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PostInfo postInfo) {
        Log.i("接收消息：", postInfo.toString());
        Toast.makeText(RetrifitMainActivity.this, postInfo.toString(), Toast.LENGTH_SHORT).show();
    }


    private void rxRequest() {

        Retrofit.Builder builder = new Retrofit.Builder();
        Retrofit retrofit = builder.baseUrl(Constant.SERVER_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(RetrofitUtils.getOkHttpClient())
                .build();

        RetrofitService service = retrofit.create(RetrofitService.class);
        Observable<PostInfo> observable = service.getPostInfoRx("yuantong", "11111111111");

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PostInfo>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(PostInfo value) {
                        Log.i("http返回：", value.toString());
                        Toast.makeText(RetrifitMainActivity.this, value.toString(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    private void baseRequest() {

        Retrofit.Builder builder = new Retrofit.Builder();
        Retrofit retrofit = builder.baseUrl(Constant.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(RetrofitUtils.getOkHttpClient())
                .build();

        RetrofitService service = retrofit.create(RetrofitService.class);
        Call<PostInfo> call = service.getPostInfo("yuantong", "11111111111");

        call.enqueue(new Callback<PostInfo>() {
            @Override
            public void onResponse(Call<PostInfo> call, Response<PostInfo> response) {
                Log.i("http返回：", response.body().toString());
                Toast.makeText(RetrifitMainActivity.this, response.body().toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<PostInfo> call, Throwable t) {
                Toast.makeText(RetrifitMainActivity.this, "-->" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }
}
