package com.neo.androidcommon.retrofitDemo.manger;

import com.neo.androidcommon.retrofitDemo.constant.Constant;
import com.neo.androidcommon.retrofitDemo.entity.BaseInfoBean;
import com.neo.androidcommon.retrofitDemo.entity.PostInfo;

import org.greenrobot.eventbus.EventBus;


public class ReceiveMessageManager {

    private static ReceiveMessageManager manager;

    public static ReceiveMessageManager getInstance() {
        return manager == null ? manager = new ReceiveMessageManager() : manager;
    }

    private ReceiveMessageManager() {
    }

    /**
     * 分发消息
     *
     * @param baseBean  Bean基类
     * @param urlOrigin 请求地址
     */
    public void dispatchMessage(BaseInfoBean baseBean, String urlOrigin) {
        switch (urlOrigin) {
            case Constant.UrlOrigin.get_post_info:
                PostInfo postInfo = (PostInfo) baseBean;
                EventBus.getDefault().post(postInfo);
                break;

            default:
                break;
        }
    }
}
