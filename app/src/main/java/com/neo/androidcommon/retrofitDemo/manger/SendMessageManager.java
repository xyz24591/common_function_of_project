package com.neo.androidcommon.retrofitDemo.manger;

import com.neo.androidcommon.retrofitDemo.constant.Constant;
import com.neo.androidcommon.retrofitDemo.entity.PostInfo;
import com.neo.androidcommon.retrofitDemo.impl.RetrofitService;
import com.neo.androidcommon.retrofitDemo.net.HttpChannel;

import io.reactivex.Observable;

/**
 * Created by Administrator on 2018/10/16.
 */

public class SendMessageManager {

    private static SendMessageManager manager;
    private HttpChannel httpChannel;
    private RetrofitService retrofitService;

    public static SendMessageManager getInstance() {


        return manager == null ? new SendMessageManager() : manager;
    }


    private SendMessageManager() {
        httpChannel = HttpChannel.getInstance();
        retrofitService = httpChannel.getService();

    }

    /**
     * @param type
     * @param postid </p>
     *               用 回调 更好 目测
     */
    public void getPostInfo(String type, String postid) {

        Observable<PostInfo> observable = retrofitService.getPostInfoRx(type, postid);

        httpChannel.sendMessage(observable, Constant.UrlOrigin.get_post_info);


    }


}
