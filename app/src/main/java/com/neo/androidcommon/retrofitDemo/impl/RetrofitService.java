package com.neo.androidcommon.retrofitDemo.impl;

import com.neo.androidcommon.retrofitDemo.constant.Constant;
import com.neo.androidcommon.retrofitDemo.entity.PostInfo;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by Administrator on 2018/10/16.
 * <p>
 * 请求参数 接口
 */

public interface RetrofitService {

    /**
     * 获取快递信息
     *
     * @param type   快递类型
     * @param postid 快递单号
     * @return Call<PostInfo>
     */
    @GET(Constant.UrlOrigin.get_post_info)
    Call<PostInfo> getPostInfo(@Query("type") String type, @Query("postid") String postid);

    /**
     * 获取快递信息
     * Rx方式
     *
     * @param type   快递类型
     * @param postid 快递单号
     * @return Observable<PostInfo>
     */
    @GET(Constant.UrlOrigin.get_post_info)
    Observable<PostInfo> getPostInfoRx(@Query("type") String type, @Query("postid") String postid);


    /**
     * 请求参数
     * 在getPostInfo方法的参数中使用了@Query注解，
     * 除此之外还可以使用
     *
     * @QueryMap、
     * @Path、@Body、
     * @FormUrlEncoded/@Field、
     * @Multipart/@Part、
     * @Header/@Headers
     */

    @GET("query")
    Call<PostInfo> getpostInfo(@Query("type") String type);

    //    上面的 相当于
//    @GET("query?type=type&postid=postid")
//    Call<PostInfo> getPostInfoUp(@Query("type") String type.@Query("postid") String postid);

    //    当参数很多的时候可以使用Map集合：
    @GET("query")
    Call<PostInfo> getBook(@QueryMap Map<String, String> parameters);


    //    用于 替换 url 中的 某些字段
    @GET("group/{id}/users")
    Call<List<String>> groupList(@Path("id") int groupId);

    //可以使用实体类作为请求体，Retrofit会帮我们自动转换：
    @POST("users/new")
    Call<PostInfo> createNew(@Body PostInfo info);

    //    用于传送表单数据，注意在头部需要加上@FormUrlEncoded，
// first_name代表key，first代表value：
    @FormUrlEncoded
    @POST("user/edit")
    Call<PostInfo> update(@Field("first_name") String first, @Field("last") String last);

    //    用于上传文件
    @Multipart
    @PUT
    Call<PostInfo> uodateInfo(@Part("photo") RequestBody photo, @Part("description") RequestBody description);

//    用于设置请求头：

    @GET("user")
    Call<PostInfo> getUser(@Header("Authorization") String authorization);

}
