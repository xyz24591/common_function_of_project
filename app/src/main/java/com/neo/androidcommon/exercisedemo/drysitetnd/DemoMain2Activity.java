package com.neo.androidcommon.exercisedemo.drysitetnd;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.CommonUtils.LogUtil;
import com.neo.androidcommon.R;
import com.neo.androidcommon.exercisedemo.drysister.PictureLoader;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/9/12.
 */

public class DemoMain2Activity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.img_show)
    ImageView imgShow;
    @BindView(R.id.btn_refresh)
    Button refreshBtn;
    @BindView(R.id.btn_show)
    Button showBtn;

    private ArrayList<Sister> data;
    private int curPos = 0; //当前显示的是哪一张
    private int page = 1;   //当前页数
    private PictureLoader loader;
    private SisterApi sisterApi;
    private SisterTask sisterTask;

    @Override
    public int intiLayout() {
        return R.layout.activity_demo_nd;
    }

    @Override
    public void initView() {
        sisterApi = new SisterApi();
        loader = new PictureLoader();
        data = new ArrayList<>();

        showBtn.setOnClickListener(this);
        refreshBtn.setOnClickListener(this);
    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_show:
                if (data != null && !data.isEmpty()) {
                    if (curPos > 9) {
                        curPos = 0;
                    }
                    loader.load(imgShow, data.get(curPos).getUrl());
                    curPos++;
                }
                break;
            case R.id.btn_refresh:
//                page++;
////                new SisterTask(page).execute();
//                curPos = 0;

                sisterTask = new SisterTask();
                sisterTask.execute();
                curPos = 0;

                break;
            default:
                break;
        }

    }

    private class SisterTask extends AsyncTask<Void, Void, ArrayList<Sister>> {
//        private int page;
//
//        public SisterTask(int page) {
//            this.page = page;
//        }

        public SisterTask() {
        }

        @Override
        protected ArrayList<Sister> doInBackground(Void... params) {
//            Toast.makeText(DemoMain2Activity.this, "页数:" + page, Toast.LENGTH_SHORT).show();
            LogUtil.e("页数:" + page);
            return sisterApi.fetchSister(10, page);
        }

        @Override
        protected void onPostExecute(ArrayList<Sister> sisters) {
            super.onPostExecute(sisters);
            data.clear();
            data.addAll(sisters);
            page++;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sisterTask.cancel(true);
    }
}
