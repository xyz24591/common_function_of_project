package com.neo.androidcommon.exercisedemo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.CommonUtils.LogUtil;
import com.neo.androidcommon.R;
import com.neo.androidcommon.exercisedemo.drysister.PictureLoader;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/9/12.
 */

public class DemoMainActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.img_show)
    ImageView imgShow;
    @BindView(R.id.btn_show)
    Button btnShow;

    private ArrayList<String> urls;
    private int curPos = 0;
    private PictureLoader loader;

    @Override
    public int intiLayout() {
        return R.layout.activity_demo_main;
    }

    @Override
    public void initView() {
        loader = new PictureLoader();
        urls = new ArrayList<>();
        urls.add("http://ww4.sinaimg.cn/large/610dc034jw1f6ipaai7wgj20dw0kugp4.jpg");
        urls.add("http://ww3.sinaimg.cn/large/610dc034jw1f6gcxc1t7vj20hs0hsgo1.jpg");
        urls.add("http://ww4.sinaimg.cn/large/610dc034jw1f6f5ktcyk0j20u011hacg.jpg");
        urls.add("http://ww1.sinaimg.cn/large/610dc034jw1f6e1f1qmg3j20u00u0djp.jpg");
        urls.add("http://ww3.sinaimg.cn/large/610dc034jw1f6aipo68yvj20qo0qoaee.jpg");
        urls.add("http://ww3.sinaimg.cn/large/610dc034jw1f69c9e22xjj20u011hjuu.jpg");
        urls.add("http://ww3.sinaimg.cn/large/610dc034jw1f689lmaf7qj20u00u00v7.jpg");
        urls.add("http://ww3.sinaimg.cn/large/c85e4a5cjw1f671i8gt1rj20vy0vydsz.jpg");
        urls.add("http://ww2.sinaimg.cn/large/610dc034jw1f65f0oqodoj20qo0hntc9.jpg");
        urls.add("http://ww2.sinaimg.cn/large/c85e4a5cgw1f62hzfvzwwj20hs0qogpo.jpg");

        btnShow.setOnClickListener(this);

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_show:
                LogUtil.i("-前-位置->" + curPos + ";;" + urls.size());

                if (curPos >= urls.size()) {
                    curPos = 0;
                }
                loader.load(imgShow, urls.get(curPos));
                curPos++;
                LogUtil.i("-后-位置->" + curPos + ";;" + urls.size());
                break;
            default:
                break;
        }


    }
}
