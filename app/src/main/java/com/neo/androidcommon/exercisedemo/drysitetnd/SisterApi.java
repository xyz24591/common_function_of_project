package com.neo.androidcommon.exercisedemo.drysitetnd;

import com.neo.androidcommon.CommonUtils.LogUtil;
import com.neo.androidcommon.exercisedemo.util.GsonUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Administrator on 2018/9/12.
 * <p>
 * 描述：网络请求处理相关类
 */

public class SisterApi {

    private static final String TAG = "Network";
    private static final String BASE_URL = "https://gank.io/api/data/福利/";

    public ArrayList<Sister> fetchSister(int count, int page) {
        String fetchUrl = BASE_URL + count + "/" + page;
        LogUtil.d("网址："+fetchUrl);
        ArrayList<Sister> sisters = new ArrayList<>();
        try {
            URL url = new URL(fetchUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            conn.setRequestMethod("GET");
            int code = conn.getResponseCode();
            LogUtil.d("Server response：" + code);
            if (code == 200) {
                InputStream in = conn.getInputStream();
                byte[] data = readFromStream(in);
                String result = new String(data, "UTF-8");
                sisters = parseSister(result);

                LogUtil.d("-解析的数据-："+ GsonUtil.GsonString(sisters));
            } else {
                LogUtil.e("请求失败：" + code);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sisters;
    }

    private ArrayList<Sister> parseSister(String result) throws JSONException {

        ArrayList<Sister> sisters = new ArrayList<>();

        JSONObject object = new JSONObject(result);
        JSONArray array = object.getJSONArray("results");
        for (int i = 0; i < array.length(); i++) {
            JSONObject results = (JSONObject) array.get(i);
            Sister sister = new Sister();
            sister.set_id(results.getString("_id"));
            sister.setCreateAt(results.getString("createdAt"));
            sister.setDesc(results.getString("desc"));
            sister.setPublishedAt(results.getString("publishedAt"));
            sister.setSource(results.getString("source"));
            sister.setType(results.getString("type"));
            sister.setUrl(results.getString("url"));
            sister.setUsed(results.getBoolean("used"));
            sister.setWho(results.getString("who"));
            sisters.add(sister);
        }
        return sisters;

    }


    public byte[] readFromStream(InputStream inputStream) {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        byte[] bytes = new byte[1024];
        int len;

        try {
            while ((len = inputStream.read(bytes)) != -1) {

                outputStream.write(bytes, 0, len);

            }
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputStream.toByteArray();
    }


}
