package com.neo.androidcommon.exercisedemo.drysiterrd;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;
import com.neo.androidcommon.exercisedemo.crash.CrashHandler;
import com.neo.androidcommon.exercisedemo.drysister.PictureLoader;
import com.neo.androidcommon.exercisedemo.drysiterrd.db.SisterDBHelper;
import com.neo.androidcommon.exercisedemo.drysiterrd.entity.Sister;
import com.neo.androidcommon.exercisedemo.drysiterrd.loader.SisterLoader;
import com.neo.androidcommon.exercisedemo.drysiterrd.network.SisterApi;
import com.neo.androidcommon.exercisedemo.drysiterrd.utils.NetworkUtils;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Administrator on 2018/9/13.
 */

public class DemoMain3Activity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.img_show)
    ImageView imgShow;
    @BindView(R.id.btn_previous)
    Button btnPrevious;
    @BindView(R.id.btn_next)
    Button btnNext;

    private ArrayList<Sister> data;
    private int curPos = 0; //当前显示的是哪一张
    private int page = 1;   //当前页数
    private PictureLoader loader;
    private SisterApi sisterApi;
    private SisterTask sisterTask;
    private SisterLoader mLoader;
    private SisterDBHelper mDbHelper;
    @Override
    public int intiLayout() {

        CrashHandler.getInstance().init(this);

        return R.layout.activity_demo_rd;
    }

    @Override
    public void initView() {

        sisterApi = new SisterApi();
        loader = new PictureLoader();
        mLoader = SisterLoader.getInstance(this);
        mDbHelper = SisterDBHelper.getInstance(this);
        data = new ArrayList<>();
        sisterTask = new SisterTask();
        sisterTask.execute();

        btnPrevious.setOnClickListener(this);
        btnNext.setOnClickListener(this);

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_previous:
                --curPos;
                if (curPos == 0) {
                    btnPrevious.setVisibility(View.INVISIBLE);
                }
                if (curPos == data.size() - 1) {
                    sisterTask = new SisterTask();
                    sisterTask.execute();
                } else if(curPos < data.size()) {
                    mLoader.bindBitmap(data.get(curPos).getUrl(), imgShow, 400, 400);
                }
                break;
            case R.id.btn_next:
                btnPrevious.setVisibility(View.VISIBLE);
                if(curPos < data.size()) {
                    ++curPos;
                }
                if (curPos > data.size() - 1) {
                    sisterTask = new SisterTask();
                    sisterTask.execute();
                } else if(curPos < data.size()){
                    mLoader.bindBitmap(data.get(curPos).getUrl(), imgShow, 400, 400);
                }
                break;
            default:
                break;
        }
    }


    private class SisterTask extends AsyncTask<Void, Void, ArrayList<Sister>> {

        SisterTask() {}

        @Override
        protected ArrayList<Sister> doInBackground(Void... params) {
            ArrayList<Sister> result = new ArrayList<>();
            if (page < (curPos + 1) / 10 + 1) {
                ++page;
            }
            //判断是否有网络
            if (NetworkUtils.isAvailable(getApplicationContext())) {
                result = sisterApi.fetchSister(10, page);
                //查询数据库里有多少个妹子，避免重复插入
                if(mDbHelper.getSistersCount() / 10 < page) {
                    mDbHelper.insertSisters(result);
                }
            } else {
                result.clear();
                result.addAll(mDbHelper.getSistersLimit(page - 1, 10));
            }
            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Sister> sisters) {
            super.onPostExecute(sisters);
            data.addAll(sisters);
            if (data.size() > 0 && curPos + 1 < data.size()) {
                mLoader.bindBitmap(data.get(curPos).getUrl(), imgShow, 400, 400);
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            sisterTask = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (sisterTask != null) {
            sisterTask.cancel(true);
        }
    }


}
