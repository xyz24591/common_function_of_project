package com.neo.androidcommon.SingleAndMultiChoiceRecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.neo.androidcommon.R;

/**
 * Created by Administrator on 2018/5/5.
 */

public class SingleAndMultiChoiceActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_multi_choice_layout);
    }

    /**
     * 单选
     * @param view
     */
    public void single(View view){
        startActivity(new Intent(this, SingleActivity.class));
    }

    /**
     * 多选
     * @param view
     */
    public void multi(View view){
        startActivity(new Intent(this, MultiActivity.class));
    }



}
