package com.neo.androidcommon.bitmapdemo.gamepaint;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/9/11.
 */

public class GamePaintActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    @BindView(R.id.img_choose)
    ImageView imgChoose;
    @BindView(R.id.btn_choose)
    Button btnChoose;
    @BindView(R.id.gay_choose)
    Gallery gayChoose;

    private int index = 0;
    private MeiziAdapter mAdapter = null;
    private int[] imageIds = new int[]
            {
                    R.mipmap.pre1, R.mipmap.pre2, R.mipmap.pre3, R.mipmap.pre4,
                    R.mipmap.pre5, R.mipmap.pre6, R.mipmap.pre7, R.mipmap.pre8,
                    R.mipmap.pre9, R.mipmap.pre10, R.mipmap.pre11, R.mipmap.pre12,
                    R.mipmap.pre13, R.mipmap.pre14, R.mipmap.pre15, R.mipmap.pre16,
                    R.mipmap.pre17, R.mipmap.pre18, R.mipmap.pre19, R.mipmap.pre20,
                    R.mipmap.pre21
            };


    @Override
    public int intiLayout() {
        return R.layout.activity_game_paint;
    }

    @Override
    public void initView() {
        mAdapter = new MeiziAdapter(this, imageIds);
        gayChoose.setAdapter(mAdapter);
        gayChoose.setOnItemSelectedListener(this);
        btnChoose.setOnClickListener(this);
    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        imgChoose.setImageResource(imageIds[position]);
        index = position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {

        Intent it = new Intent(this, CaClothes.class);
        Bundle bundle = new Bundle();
        bundle.putCharSequence("num", Integer.toString(index));
        it.putExtras(bundle);
        startActivity(it);

    }
}
