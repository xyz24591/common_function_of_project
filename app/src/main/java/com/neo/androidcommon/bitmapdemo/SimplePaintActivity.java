package com.neo.androidcommon.bitmapdemo;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

/**
 * Created by Administrator on 2018/9/11.
 */

public class SimplePaintActivity extends BaseActivity {
    @Override
    public int intiLayout() {
        return R.layout.activity_simple_paint;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }
}
