package com.neo.androidcommon.EventBusDemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2018/5/22.
 * <p>
 *
 * EventBus 首页
 */


public class EventBusMainActivity extends BaseActivity {
    @BindView(R.id.tv_message)
    TextView tvMessage;
    @BindView(R.id.bt_message)
    Button btMessage;
    @BindView(R.id.bt_subscription)
    Button btSubscription;

    @Override
    public int intiLayout() {
        return R.layout.eventbus_layout;
    }

    @Override
    public void initView() {

        tvMessage.setText(TAG + "Main");

        btSubscription.setText("订阅事件");

        btMessage.setText("跳转到EventBusSecondActivity");


    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }


    @OnClick({R.id.bt_message, R.id.bt_subscription})
    public void OnClick(View view) {

        switch (view.getId()) {

            case R.id.bt_message:

                startActivity(new Intent(EventBusMainActivity.this, EventBusSecondActivity.class));
                break;
            case R.id.bt_subscription:
                //注册事件
                if (EventBus.getDefault().isRegistered(EventBusMainActivity.this)) {
                    showShortToast("事件已注册");
                } else {
                    EventBus.getDefault().register(EventBusMainActivity.this);
                }
                break;


        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(EventBusMainActivity.this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNeoEvent(MessageEvent messageEvent) {

        tvMessage.setText(messageEvent.getMessage());

    }

    @Subscribe(sticky = true)
    public void onNeoStickyEvent(MessageEvent messageEvent) {

        tvMessage.setText(messageEvent.getMessage());

    }

}
