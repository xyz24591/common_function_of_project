package com.neo.androidcommon.EventBusDemo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2018/5/22.
 */

public class EventBusSecondActivity extends BaseActivity {
    @BindView(R.id.tv_message)
    TextView tvMessage;
    @BindView(R.id.bt_message)
    Button btMessage;
    @BindView(R.id.bt_subscription)
    Button btSubscription;

    @Override
    public int intiLayout() {
        return R.layout.eventbus_layout;
    }

    @Override
    public void initView() {

        tvMessage.setText(TAG + "second");

        btSubscription.setText("发送粘性事件");

        btMessage.setText("发送事件");
    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }


    @OnClick({R.id.bt_subscription, R.id.bt_message})
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.bt_subscription:
                EventBus.getDefault().postSticky(new MessageEvent("粘性事件"));
                finish();
                break;

            case R.id.bt_message:

                EventBus.getDefault().post(new MessageEvent("我是 从第二个页面传递过来的值"));
                finish();
                break;


        }

    }


}
