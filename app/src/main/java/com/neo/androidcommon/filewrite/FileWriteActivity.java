package com.neo.androidcommon.filewrite;

import android.app.usage.ExternalStorageStats;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.CommonUtils.LogUtil;
import com.neo.androidcommon.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/9/6.
 */

public class FileWriteActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.editname)
    EditText editname;
    @BindView(R.id.editdetail)
    EditText editdetail;
    @BindView(R.id.btnsave)
    Button btnsave;
    @BindView(R.id.btnsavesd)
    Button btnsavesd;
    @BindView(R.id.btnradsd)
    Button btnradsd;
    @BindView(R.id.btnclean)
    Button btnclean;
    @BindView(R.id.btnread)
    Button btnread;
    @BindView(R.id.LinearLayout1)
    LinearLayout LinearLayout1;

    @Override
    public int intiLayout() {
        return R.layout.activity_file_write;
    }

    @Override
    public void initView() {
        btnclean.setOnClickListener(this);
        btnsave.setOnClickListener(this);
        btnread.setOnClickListener(this);
        btnsavesd.setOnClickListener(this);
        btnradsd.setOnClickListener(this);
    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

        InputStream inputStream = getResources().openRawResource(0);

        AssetManager assets = getAssets();

        try {
            InputStream filename = assets.open("filename");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnclean:
                editdetail.setText("");
                editname.setText("");
                break;
            case R.id.btnsavesd:

                String filename1 = editname.getText().toString();
                String filedetail1 = editdetail.getText().toString();

                try {
                    savaFileToSD(filename1, filedetail1);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "写入异常", Toast.LENGTH_SHORT).show();
                }


                break;
            case R.id.btnradsd:
                String filenamer = editname.getText().toString();

                try {
                    String readFromSdCard = readFromSD(filenamer);

                    Toast.makeText(this, "-->" + readFromSdCard, Toast.LENGTH_SHORT).show();

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "-读取失败->", Toast.LENGTH_SHORT).show();

                }

                break;

            case R.id.btnsave:

                FileHelper fileHelper = new FileHelper(FileWriteActivity.this);
                String filename = editname.getText().toString();
                String filedetail = editdetail.getText().toString();

                try {
                    fileHelper.save(filename, filedetail);
                    Toast.makeText(getApplicationContext(), "数据写入成功", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Toast.makeText(getApplicationContext(), "数据写入失败", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

                break;
            case R.id.btnread:
                String detail = "";
                FileHelper fileHelper2 = new FileHelper(FileWriteActivity.this);

                try {
                    String fname = editname.getText().toString();
                    detail = fileHelper2.read(fname);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getApplicationContext(), detail, Toast.LENGTH_SHORT).show();
                break;


            default:
                break;

        }


    }

    private String readFromSD(String filenamer) throws IOException {
        StringBuilder sb = new StringBuilder("");
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {

            filenamer = Environment.getExternalStorageDirectory().getCanonicalPath() + File.separator + filenamer;
            LogUtil.i("-读取历经-》" + filenamer);
            FileInputStream fileInputStream = new FileInputStream(filenamer);

            byte[] bytes = new byte[1024];
            int len = 0;

            while ((len = fileInputStream.read(bytes)) > 0) {

                sb.append(new String(bytes, 0, len));

            }
            fileInputStream.close();

        }
        return sb.toString();

    }

    private void savaFileToSD(String filename, String filedetail) throws IOException {

        //如果手机已插入sd卡,且app具有读写sd卡的权限
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {

            filename = Environment.getExternalStorageDirectory().getCanonicalPath() + File.separator + filename;
            LogUtil.i("-写入路径-》" + filename);

            //这里就不要用openFileOutput了,那个是往手机内存中写数据的
            FileOutputStream outputStream = new FileOutputStream(filename);
            outputStream.write(filedetail.getBytes());

            outputStream.close();
            Toast.makeText(this, "写入 完毕", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "写入失败", Toast.LENGTH_SHORT).show();
        }


    }
}
