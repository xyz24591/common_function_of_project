package com.neo.androidcommon.filewrite;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Administrator on 2018/9/6.
 */

public class FileHelper {

    private Context mContext;

    public FileHelper() {
    }

    public FileHelper(Context context) {
        super();
        this.mContext = context;
    }


    /**
     * 文件保存的方法
     */
    public void save(String fileName, String fileContent) throws IOException {

//    使用私有模式 创建出来的应用文件 只能被本应用 访问 还能 覆盖源文件 哟

        FileOutputStream output = mContext.openFileOutput(fileName, Context.MODE_PRIVATE);

        output.write(fileContent.getBytes()); // 将 String 以 字节流的方式 写入
        output.close();// 关闭 输入流
    }


    public String read(String fileName) throws IOException {

        FileInputStream input = mContext.openFileInput(fileName);

        byte[] bytes = new byte[1024];
        StringBuilder stringBuilder = new StringBuilder();
        int len = 0;

        while ((len = input.read(bytes)) > 0) {

            stringBuilder.append(new String(bytes, 0, len));


        }
        input.close();
        return stringBuilder.toString();
    }


}
