package com.neo.androidcommon.Activity;

import android.content.DialogInterface;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/7/26.
 */

public class GestureViewActivity extends BaseActivity {
    @BindView(R.id.gesture)
    GestureOverlayView gesture;

    @Override
    public int intiLayout() {
        return R.layout.activity_gesture_view;
    }

    @Override
    public void initView() {

        gesture.setGestureColor(Color.GREEN);
        gesture.setGestureStrokeWidth(5);
        gesture.addOnGesturePerformedListener(new GestureOverlayView.OnGesturePerformedListener() {
            @Override
            public void onGesturePerformed(GestureOverlayView overlay, final Gesture gesture) {
                View saveDialog = getLayoutInflater().inflate(R.layout.dialog_save, null, false);
                ImageView img_show = (ImageView) saveDialog.findViewById(R.id.img_show);
                final EditText edit_name = (EditText) saveDialog.findViewById(R.id.edit_name);
                Bitmap bitmap = gesture.toBitmap(128, 128, 10, Color.CYAN);
                img_show.setImageBitmap(bitmap);
                new AlertDialog.Builder(GestureViewActivity.this).setView(saveDialog)
                        .setTitle("保存手势")
                        .setPositiveButton("保存", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //获取文件对应的手势库
                                GestureLibrary library = GestureLibraries.fromFile("/mnt/sdcard/mygestures");
                                library.addGesture(edit_name.getText().toString(), gesture);
                                library.save();
                            }
                        }).setNegativeButton("取消", null).show();


            }
        });

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }

}
