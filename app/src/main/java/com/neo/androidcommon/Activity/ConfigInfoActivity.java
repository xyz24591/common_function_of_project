package com.neo.androidcommon.Activity;

import android.content.res.Configuration;
import android.widget.TextView;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import butterknife.BindView;

/**
 * Created by Administrator on 2018/7/26.
 */

public class ConfigInfoActivity extends BaseActivity {
    @BindView(R.id.config_info_tv)
    TextView configInfoTv;

    @Override
    public int intiLayout() {
        return R.layout.activity_config;
    }

    @Override
    public void initView() {
        StringBuffer status = new StringBuffer();
        //①获取系统的Configuration对象
        Configuration cfg = getResources().getConfiguration();
        //②想查什么查什么
        status.append("屏幕密度densityDpi:" + cfg.densityDpi + "\n");
        status.append("字体的缩放因子fontScale:" + cfg.fontScale + "\n");
        status.append("hardKeyboardHidden:" + cfg.hardKeyboardHidden + "\n");
        status.append("keyboard:" + cfg.keyboard + "\n");
        status.append("keyboardHidden:" + cfg.keyboardHidden + "\n");
        status.append("当前的语言环境locale:" + cfg.locale + "\n");
        status.append("移动信号的国家码mcc:" + cfg.mcc + "\n");
        status.append("移动信号的网络码mnc:" + cfg.mnc + "\n");
        status.append("系统上方向导航设备的类型navigation:" + cfg.navigation + "\n");
        status.append("获取系统屏幕的方向navigationHidden:" + cfg.navigationHidden + "\n");
        status.append("orientation:" + cfg.orientation + "\n");
        status.append("screenHeightDp:" + cfg.screenHeightDp + "\n");
        status.append("screenWidthDp:" + cfg.screenWidthDp + "\n");
        status.append("screenLayout:" + cfg.screenLayout + "\n");
        status.append("smallestScreenWidthDp:" + cfg.densityDpi + "\n");
        status.append("touchscreen:" + cfg.densityDpi + "\n");
        status.append("uiMode:" + cfg.densityDpi + "\n");
        configInfoTv.setText(status.toString());
    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }


}
