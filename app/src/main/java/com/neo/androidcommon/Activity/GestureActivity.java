package com.neo.androidcommon.Activity;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

/**
 * Created by Administrator on 2018/7/26.
 */

public class GestureActivity extends BaseActivity {

    private GestureDetector mDetector;
    private MyGestureListener mgListener;
    private final static int MIN_MOVE = 200;   //最小距离

    @Override
    public int intiLayout() {
        return R.layout.activity_gesture;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {
        mgListener = new MyGestureListener();
        mDetector = new GestureDetector(this, mgListener);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mDetector.onTouchEvent(event);
    }


    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

            if (e1.getY() - e2.getY() > MIN_MOVE) {
                Toast.makeText(GestureActivity.this, "启动", Toast.LENGTH_SHORT).show();
            } else if (e1.getY() - e2.getY() < MIN_MOVE) {

                Toast.makeText(GestureActivity.this, "关闭", Toast.LENGTH_SHORT).show();

            }

//            return super.onFling(e1, e2, velocityX, velocityY);
            return true;
        }
    }


//    //自定义一个GestureListener,这个是View类下的，别写错哦！！！  原始的
//    private class MyGestureListener implements GestureDetector.OnGestureListener {
//
//
//        @Override
//        public boolean onDown(MotionEvent motionEvent) {
//            Log.d(TAG, "onDown:按下");
//            return false;
//        }
//
//        @Override
//        public void onShowPress(MotionEvent motionEvent) {
//            Log.d(TAG, "onShowPress:手指按下一段时间,不过还没到长按");
//        }
//
//        @Override
//        public boolean onSingleTapUp(MotionEvent motionEvent) {
//            Log.d(TAG, "onSingleTapUp:手指离开屏幕的一瞬间");
//            return false;
//        }
//
//        @Override
//        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
//            Log.d(TAG, "onScroll:在触摸屏上滑动");
//            return false;
//        }
//
//        @Override
//        public void onLongPress(MotionEvent motionEvent) {
//            Log.d(TAG, "onLongPress:长按并且没有松开");
//        }
//
//        @Override
//        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
//            Log.d(TAG, "onFling:迅速滑动，并松开");
//            return false;
//        }
//    }

}
