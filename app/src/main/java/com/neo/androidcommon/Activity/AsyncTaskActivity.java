package com.neo.androidcommon.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.neo.androidcommon.Activity.task.MyAsyncTask;
import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/7/26.
 */

public class AsyncTaskActivity extends BaseActivity {
    @BindView(R.id.config_info_tv)
    TextView configInfoTv;
    @BindView(R.id.pgbar)
    ProgressBar pgbar;
    @BindView(R.id.btnupdate)
    Button btnupdate;

    @Override
    public int intiLayout() {
        return R.layout.activity_config;
    }

    @Override
    public void initView() {
//        configInfoTv.setText("开始执行异步任务");

        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyAsyncTask asyncTask = new MyAsyncTask(configInfoTv, pgbar);
                asyncTask.execute(1000);
            }
        });


    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }

}
