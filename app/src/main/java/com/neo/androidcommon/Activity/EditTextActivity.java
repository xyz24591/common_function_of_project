package com.neo.androidcommon.Activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.CommonUtils.UIUtils;
import com.neo.androidcommon.R;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Administrator on 2018/5/18.
 */

public class EditTextActivity extends BaseActivity {
    @BindView(R.id.add_face_btn)
    Button addFaceBtn;
    @BindView(R.id.input_et)
    EditText inputEt;

    @Override
    public int intiLayout() {
        return R.layout.editext_layout;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }


    @OnClick({R.id.add_face_btn})
    public void OnClick(View view) {
        switch (view.getId()) {

            case R.id.add_face_btn:

                SpannableString spanStr = new SpannableString("imge");
                Drawable drawable = UIUtils.getDrawable(R.drawable.and_icon);
                drawable.setBounds(0,0,drawable.getIntrinsicWidth(),drawable.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(drawable,ImageSpan.ALIGN_BASELINE);
                spanStr.setSpan(span,0,4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                int cursor = inputEt.getSelectionStart();
                inputEt.getText().insert(cursor, spanStr);

                break;


        }

    }

}
