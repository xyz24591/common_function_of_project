package com.neo.androidcommon.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.graphics.Color;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018/7/26.
 */

public class GetstureViewGetActivity extends BaseActivity {

    private Context mContext;
    private GestureLibrary gestureLibrary;
    private GestureOverlayView gesture;

    @Override
    public int intiLayout() {
        mContext = GetstureViewGetActivity.this;
        return R.layout.activity_gesture_view;
    }

    @Override
    public void initView() {

        gestureLibrary = GestureLibraries.fromFile("mmt/sdcard/mygestures");

        //获取手势编辑组件后，设置相关参数
        gesture = (GestureOverlayView) findViewById(R.id.gesture);
        gesture.setGestureColor(Color.GREEN);
        gesture.setGestureStrokeWidth(5);

        gesture.addOnGesturePerformedListener(new GestureOverlayView.OnGesturePerformedListener() {
            @Override
            public void onGesturePerformed(GestureOverlayView gestureOverlayView, final Gesture gesture) {
                //识别用户刚绘制的手势
                ArrayList<Prediction> predictions = gestureLibrary.recognize(gesture);
                ArrayList<String> result = new ArrayList<String>();
                //遍历所有找到的Prediction对象
                for (Prediction pred : predictions) {
                    if (pred.score > 2.0) {
                        result.add("与手势【" + pred.name + "】相似度为" + pred.score);
                    }
                }
                if (result.size() > 0) {
                    ArrayAdapter<Object> adapter = new ArrayAdapter<Object>(mContext,
                            android.R.layout.simple_dropdown_item_1line, result.toArray());
                    new AlertDialog.Builder(mContext).setAdapter(adapter,null).setPositiveButton("确定",null).show();
                }else{
                    Toast.makeText(mContext,"无法找到匹配的手势！",Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }
}
