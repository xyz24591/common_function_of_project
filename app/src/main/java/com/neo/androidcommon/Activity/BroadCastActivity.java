package com.neo.androidcommon.Activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.BroadCast.MyBRReceiver;
import com.neo.androidcommon.R;

/**
 * Created by Administrator on 2018/7/31.
 */

public class BroadCastActivity extends BaseActivity {

    private MyBRReceiver myBRReceiver;

    @Override
    public int intiLayout() {
        return R.layout.activity_broadcast;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }


    @Override
    protected void initListener() {

    }


    public void RegisterBro(View view) {
        initBroadCastReceiver();

    }


    private void initBroadCastReceiver() {

        myBRReceiver = new MyBRReceiver();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(myBRReceiver, intentFilter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (myBRReceiver != null) {
            unregisterReceiver(myBRReceiver);
        }
    }
}
