package com.neo.androidcommon.Activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.CommonUtils.LogUtil;
import com.neo.androidcommon.Interfacepro.OnItemClickListener;
import com.neo.androidcommon.R;
import com.neo.androidcommon.adapter.DividerItemDecoration;
import com.neo.androidcommon.adapter.RecAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/10/8.
 */

public class RecyclerViewActivity extends BaseActivity {
    @BindView(R.id.rec)
    RecyclerView mRecyclerView;
    private ArrayList<String> datas;

    @Override
    public int intiLayout() {
        return R.layout.activity_recycler;
    }

    @Override
    public void initView() {

        if (datas == null) {
            datas = new ArrayList<>();
        }

        for (int i = 0; i < 10; i++) {

            datas.add("我是 第" + i + "条 数据");
        }
//设置 布局管理器


//        列表 管理器
//        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
////        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        mRecyclerView.setLayoutManager(layoutManager);

//        GrideView 列表展现

        mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL));

//        mRecyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL_LIST));
//设置 item 增加和 删除动画
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        RecAdapter recAdapter = new RecAdapter(datas, this);
        mRecyclerView.setAdapter(recAdapter);

        recAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int postion) {
                Toast.makeText(RecyclerViewActivity.this, "短按" + postion, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemLongClick(View view, int position) {
                Toast.makeText(RecyclerViewActivity.this, "长按" + position, Toast.LENGTH_SHORT).show();

            }
        });


    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }

}
