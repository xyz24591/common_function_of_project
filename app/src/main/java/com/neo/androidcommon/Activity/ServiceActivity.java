package com.neo.androidcommon.Activity;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;
import com.neo.androidcommon.service.LongRunningService;
import com.neo.androidcommon.service.MyBindService;
import com.neo.androidcommon.service.MyIntentService;
import com.neo.androidcommon.service.WeatherService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2018/7/30.
 */

public class ServiceActivity extends BaseActivity {
    @BindView(R.id.server_start_btn)
    Button serverStartBtn;
    @BindView(R.id.server_force_groud_btn)
    Button serverForceGroudBtn;
    @BindView(R.id.server_stop_btn)
    Button serverStopBtn;

    Intent intent;
    @BindView(R.id.server_start_bind_btn)
    Button serverStartBindBtn;
    @BindView(R.id.server_start_unbind_btn)
    Button serverStartUnbindBtn;
    @BindView(R.id.server_getstate_btn)
    Button serverGetstateBtn;
    @BindView(R.id.server_intent_service_btn)
    Button serverIntentServiceBtn;
    @BindView(R.id.server_longrun_service_btn)
    Button serverLongRunService_btn;

    Intent intentBindService;
    Intent intentLongRunService;

    MyBindService.MyBinder myBinder;
    private ServiceConnection connection = new ServiceConnection() {

        //Activity与Service断开连接时回调该方法

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i(TAG, "onServiceConnected: 服务 链接.onServiceConnected..");
            myBinder = (MyBindService.MyBinder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i(TAG, "onServiceDisconnected: 服务 断开.onServiceDisconnected..");
        }
    };

    @Override
    public int intiLayout() {
        return R.layout.activity_service;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

        //创建启动Service的Intent,以及Intent属性
        intent = new Intent();
        intent.setAction("com.neo.androidcommon.service.MyService");
        intent.setPackage(getPackageName());

//或者
//        intent = new Intent(this, MyService.class);


        intentBindService = new Intent();
        intentBindService.setAction("com.neo.androidcommon.service.MyBindService");
        intentBindService.setPackage(getPackageName());

//        intentBindService = new Intent(this,MyBindService.class);

        intentLongRunService = new Intent(this, LongRunningService.class);

    }

    @Override
    protected void initListener() {

    }


    @OnClick({R.id.server_start_btn, R.id.server_force_groud_btn, R.id.server_stop_btn, R.id.server_start_bind_btn, R.id.server_start_unbind_btn, R.id.server_getstate_btn, R.id.server_intent_service_btn, R.id.server_longrun_service_btn})
    public void OnClick(View view) {

        if (intent == null) {
            return;
        }
        switch (view.getId()) {

            case R.id.server_start_btn:
                startService(intent);
                break;
            case R.id.server_stop_btn:

                stopService(intent);

                break;
            case R.id.server_start_bind_btn:

                bindService(intentBindService, connection, Service.BIND_AUTO_CREATE);

                break;
            case R.id.server_force_groud_btn:

                startService(new Intent(this, WeatherService.class));

                break;
            case R.id.server_start_unbind_btn:

                unbindService(connection);
                break;
            case R.id.server_getstate_btn:

                int count = myBinder.getCount();

                Toast.makeText(this, "->" + count, Toast.LENGTH_SHORT).show();

                break;

            case R.id.server_intent_service_btn:

                Intent it1 = new Intent(this, MyIntentService.class);
//                Intent it1 = new Intent("com.neo.androidcommon.service.MyIntentService");
                Bundle b1 = new Bundle();
                b1.putString("param", "s1");
                it1.putExtras(b1);

                Intent it2 = new Intent(this, MyIntentService.class);
//                Intent it2 = new Intent("com.neo.androidcommon.service.MyIntentService");
                Bundle b2 = new Bundle();
                b2.putString("param", "s2");
                it2.putExtras(b2);

                Intent it3 = new Intent(this, MyIntentService.class);
//                Intent it3 = new Intent("com.neo.androidcommon.service.MyIntentService");
                Bundle b3 = new Bundle();
                b3.putString("param", "s3");
                it3.putExtras(b3);

                //接着启动多次IntentService,每次启动,都会新建一个工作线程
                //但始终只有一个IntentService实例
                startService(it1);
                startService(it2);
                startService(it3);

                stopService(intentLongRunService);
                break;

            case R.id.server_longrun_service_btn:

                startService(intentLongRunService);

                break;

            default:
                break;

        }


    }

}
