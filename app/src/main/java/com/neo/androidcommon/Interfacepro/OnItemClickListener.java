package com.neo.androidcommon.Interfacepro;

import android.view.View;

/**
 * Created by Administrator on 2018/10/8.
 */

public interface OnItemClickListener {
    void onItemClick(View view, int postion);

    void onItemLongClick(View view, int position);

}
