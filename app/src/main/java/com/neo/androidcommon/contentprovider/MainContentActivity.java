package com.neo.androidcommon.contentprovider;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

/**
 * Created by Administrator on 2018/9/3.
 */

public class MainContentActivity extends BaseActivity {
    @Override
    public int intiLayout() {
        return R.layout.activity_content_main;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }

    public void getMsgs(View view) {

        Uri uri = Uri.parse("content://sms/");
        ContentResolver resolver = getContentResolver();
        Cursor cursor = resolver.query(uri, new String[]{"address", "date", "type", "body"}, null, null, null);
        while (cursor.moveToNext()) {

            String address = cursor.getString(0);
            String date = cursor.getString(1);
            String type = cursor.getString(2);
            String body = cursor.getString(3);

            Log.i(TAG, "地址:" + address);
            Log.i(TAG, "时间:" + date);
            Log.i(TAG, "类型:" + type);
            Log.i(TAG, "内容:" + body);
            Log.d(TAG, "getMsgs: =========================");
        }
        cursor.close();
        Toast.makeText(this, "读取完毕", Toast.LENGTH_SHORT).show();
    }


    public void insertMsg(View view) {
//5.0 以上 就 写不进去了
        ContentResolver resolver = getContentResolver();
        Uri uri = Uri.parse("content://sms/");
        ContentValues conValues = new ContentValues();
        conValues.put("address", "123456789");
        conValues.put("type", 1);
        conValues.put("date", System.currentTimeMillis());
        conValues.put("body", "no zuo no die why you try!");
        resolver.insert(uri, conValues);

        Log.i(TAG, "insertMsg: ====插入完毕===");
    }


    public void getContacts(View view) {
//①查询raw_contacts表获得联系人的id
        ContentResolver resolver = getContentResolver();
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

        Cursor cursor = resolver.query(uri, null, null, null, null);

        while (cursor.moveToNext()) {

            String cName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String cNum = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            Log.i(TAG, "getContacts: -电话->" + cNum + "--名字-" + cName);
            Log.d(TAG, "getContacts: =========================");
        }
        cursor.close();
    }


    public void getByName(View view) {

        Log.i(TAG, "getContactsByName: ");


//        ContentResolver resolver = getContentResolver();
//        Cursor cursor = resolver.query(uri, new String[]{"dispaly_name"}, null, null, null);

        Uri uri = Uri.parse("content://com.android.contacts/data/phones/filter/" + "4008308300");
        ContentResolver resolver = getContentResolver();
        Cursor cursor = resolver.query(uri, new String[]{"display_name"}, null, null, null);

        if (cursor.moveToFirst()) {

            String name = cursor.getString(0);

            Log.i(TAG, "getContactsByName: " + name);
            Toast.makeText(this, "对应名字是" + name, Toast.LENGTH_SHORT).show();

        }
        cursor.close();

    }

}
