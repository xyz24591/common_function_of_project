package com.neo.androidcommon.mytest;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.VelocityTracker;
import android.view.View;

/**
 * Created by Administrator on 2018/9/17.
 */

public class MyView extends View {
    public MyView(Context context) {
        super(context);
    }

    public MyView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MyView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void init() {

        VelocityTracker tracker = VelocityTracker.obtain();
//        tracker.addMovement();
        tracker.computeCurrentVelocity(1000);
    }

}
