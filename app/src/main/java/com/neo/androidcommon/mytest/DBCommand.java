package com.neo.androidcommon.mytest;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.neo.androidcommon.exercisedemo.drysitetnd.Sister;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Administrator on 2018/9/15.
 */

public abstract class DBCommand<T> {
    //    数据库  执行引擎 只有一个线程的线程池
    private static ExecutorService sDbEngine = Executors.newSingleThreadExecutor();

    //    主线程 消息队列的 Handler
    private final static Handler sUIhandler = new Handler(Looper.getMainLooper());

//    执行数据库 操作

    public final void execute() {

        sDbEngine.execute(new Runnable() {
            @Override
            public void run() {
                postResult(doInBackgroung());
            }
        });

    }

    //    将 结果 投递到 UI 线程
    private void postResult(final T result) {
        sUIhandler.post(new Runnable() {
            @Override
            public void run() {
                onPostExecute(result);
            }
        });

    }

    //    后台 执行数据库操作
    protected abstract T doInBackgroung();

    //    将 结构投递到 UI 线程
    protected void onPostExecute(T t) {
    }

    class myMy {

        public class myDb extends DBCommand<Sister> {

            @Override
            protected Sister doInBackgroung() {
                return new Sister();
            }

            @Override
            protected void onPostExecute(Sister sister) {
                super.onPostExecute(sister);
            }
        }

//        new myDb().execute();


    }

}
