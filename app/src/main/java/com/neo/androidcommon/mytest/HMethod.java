package com.neo.androidcommon.mytest;

/**
 * @author Administrator
 * @date 2018/9/14
 * <p>
 * 枚举 示例
 */

public enum HMethod {
    /**
     * get
     */
    GET("GET"),
    POST("POST"),
    PUT("PUT"),
    DELETE("DELETE");

    private String mHMethod = "";

    private HMethod(String mHMethod) {
        this.mHMethod = mHMethod;
    }

    @Override
    public String toString() {
        return mHMethod;
    }
}
