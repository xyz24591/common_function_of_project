package com.neo.androidcommon.webview.javajs.entity;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.neo.androidcommon.R;

/**
 * Created by Administrator on 2018/9/10.
 */

public class MyObject {


    private Context context;
    public MyObject(Context context) {
        this.context = context;
    }

    //将显示Toast和对话框的方法暴露给JS脚本调用
    public void showToast(String name) {
        Toast.makeText(context, name, Toast.LENGTH_SHORT).show();
    }

    public void showDialog() {
        new AlertDialog.Builder(context)
                .setTitle("联系人列表").setIcon(R.mipmap.ic_launcher_round)
                .setItems(new String[]{"基神", "B神", "曹神", "街神", "翔神"}, null)
                .setPositiveButton("确定", null).create().show();
    }




}
