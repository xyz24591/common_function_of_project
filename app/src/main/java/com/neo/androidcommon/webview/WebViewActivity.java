package com.neo.androidcommon.webview;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.CommonUtils.LogUtil;
import com.neo.androidcommon.R;


import butterknife.BindView;

/**
 * Created by Administrator on 2018/9/10.
 */

public class WebViewActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.btn_back)
    Button btnBack;
    @BindView(R.id.btn_top)
    Button btnTop;
    @BindView(R.id.btn_refresh)
    Button btnRefresh;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.txt_indicator)
    TextView txtIndicator;
    @BindView(R.id.wView)
    ScrollWebView wView;
    private long exitTime = 0;

    @Override
    public int intiLayout() {
        return R.layout.activity_webview;
    }

    @Override
    public void initView() {
        btnBack.setOnClickListener(this);
        btnRefresh.setOnClickListener(this);
        btnTop.setOnClickListener(this);

        initWebView();
    }

    private void initWebView() {


        WebSettings settings = wView.getSettings();

        settings.setUseWideViewPort(true);//设定支持viewport
        settings.setLoadWithOverviewMode(true);   //自适应屏幕
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setSupportZoom(true);//设定支持缩放

        wView.loadUrl("http://www.hao123.com");
        wView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                txtTitle.setText(title);
            }


        });

        wView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                wView.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {

                CookieManager cookieManager = CookieManager.getInstance();
                String cookieStr = cookieManager.getCookie(url);
                Log.i(TAG, "onPageFinished: cookies::" + cookieStr);
                super.onPageFinished(view, url);
//               设置 cookie

                cookieManager.setAcceptCookie(true);
                cookieManager.setCookie(url, cookieStr);
//        cookieManager.flush(); // 21 以上版本
                CookieSyncManager.getInstance().sync();

            }
        });
        wView.setOnScrollChangeCallBack(new ScrollWebView.OnScrollChangedCallback() {
            @Override
            public void onScroll(int dx, int dy) {
//                LogUtil.i("----dx--" + dx + ";;" + dy);
                if (dy > 0) {
                    txtIndicator.setText("上");
                } else {
                    txtIndicator.setText("下");
                }


            }
        });


    }

    public void setCookie() {

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        cookieManager.setCookie("url", "value");
//        cookieManager.flush(); // 21 以上版本
        CookieSyncManager.getInstance().sync();
    }


    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.btn_back:
                finish();          //关闭当前Activity
                break;
            case R.id.btn_refresh:
                wView.reload();   //刷新当前页面
                break;
            case R.id.btn_top:
                wView.setScrollY(0);   //滚动到顶部
                break;
            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {

        if (wView.canGoBack()) {
            wView.goBack();
        } else {

            if ((System.currentTimeMillis() - exitTime) > 2000) {

                Toast.makeText(getApplicationContext(), "再按一次退出程序",
                        Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            } else {
                finish();
            }

        }


    }
}
