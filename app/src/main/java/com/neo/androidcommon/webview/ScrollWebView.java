package com.neo.androidcommon.webview;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * Created by Administrator on 2018/9/10.
 * <p>
 * WebView滚动事件的监听
 */

public class ScrollWebView extends WebView {
    private OnScrollChangedCallback mOnScrollChangedCallback;

    public ScrollWebView(Context context) {
        super(context);
    }

    public ScrollWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);

        if (mOnScrollChangedCallback != null) {

            mOnScrollChangedCallback.onScroll(l - oldl, t - oldt);
        }

    }

    public void setOnScrollChangeCallBack(OnScrollChangedCallback onScrollChangedCallback) {

        mOnScrollChangedCallback = onScrollChangedCallback;

    }


    public interface OnScrollChangedCallback {

        //这里的dx和dy代表的是x轴和y轴上的偏移量，你也可以自己把l, t, oldl, oldt四个参数暴露出来
        void onScroll(int dx, int dy);

    }

}
