package com.neo.androidcommon.sharedpreference;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/9/7.
 */

public class SharedPreferenceActivity extends BaseActivity {
    @BindView(R.id.editname)
    EditText editname;
    @BindView(R.id.editpasswd)
    EditText editpasswd;
    @BindView(R.id.btnlogin)
    Button btnlogin;
    @BindView(R.id.btnread)
    Button btnread;

    private String strname;
    private String strpasswd;
    private SharedHelper helper;

    @Override
    public int intiLayout() {
        return R.layout.activity_sharedpreference;
    }

    @Override
    public void initView() {
        helper = new SharedHelper(this);
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strname = editname.getText().toString();
                strpasswd = editpasswd.getText().toString();

                helper.save(strname, strpasswd);
            }
        });


        btnread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Map<String, String> data = helper.read();

                editname.setText(data.get("username"));
                editpasswd.setText(data.get("pwd"));

            }
        });

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }


}
