package com.neo.androidcommon.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/9/7.
 */

public class SharedHelper {

    private Context mContext;

    public SharedHelper() {
    }

    public SharedHelper(Context mContext) {
        this.mContext = mContext;
    }


    public void save(String name, String pwd) {

        SharedPreferences preferences = mContext.getSharedPreferences("mysp", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("username", name);
        editor.putString("pwd", pwd);
        editor.commit();
        Toast.makeText(mContext, "保存完毕", Toast.LENGTH_SHORT).show();

    }

    public Map<String, String> read() {
        Map<String, String> data = new HashMap<>();
        SharedPreferences preferences = mContext.getSharedPreferences("mysp", Context.MODE_PRIVATE);
        String username = preferences.getString("username", "");
        data.put("username", username);
        String pwd = preferences.getString("pwd", "");
        data.put("pwd", pwd);

        return data;


    }


}
