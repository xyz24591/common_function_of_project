package com.neo.androidcommon.Vibratordemo;

import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/9/12.
 */

public class VibratorActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.btn_hasVibrator)
    Button btnHasVibrator;
    @BindView(R.id.btn_short)
    Button btnShort;
    @BindView(R.id.btn_long)
    Button btnLong;
    @BindView(R.id.btn_rhythm)
    Button btnRhythm;
    @BindView(R.id.btn_cancle)
    Button btnCancle;
    private Vibrator myVibrator;

    @Override
    public int intiLayout() {
        return R.layout.activity_vibrator;
    }

    @Override
    public void initView() {

        myVibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {
        btnHasVibrator.setOnClickListener(this);
        btnShort.setOnClickListener(this);
        btnLong.setOnClickListener(this);
        btnRhythm.setOnClickListener(this);
        btnCancle.setOnClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_hasVibrator:
                Toast.makeText(this, myVibrator.hasVibrator() ? "当前设备有振动器" : "当前设备无振动器",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_short:
                myVibrator.cancel();
                myVibrator.vibrate(new long[]{100, 200, 100, 200}, 0);
                Toast.makeText(this, "短振动", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_long:
                myVibrator.cancel();
                myVibrator.vibrate(new long[]{100, 100, 100, 1000}, 0);
                Toast.makeText(this, "长振动", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_rhythm:
                myVibrator.cancel();
                myVibrator.vibrate(new long[]{500, 100, 500, 100, 500, 100}, 0);
                Toast.makeText(this, "节奏振动", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_cancle:
                myVibrator.cancel();
                Toast.makeText(this, "取消振动", Toast.LENGTH_SHORT).show();

            default:
                break;
        }

    }
}
