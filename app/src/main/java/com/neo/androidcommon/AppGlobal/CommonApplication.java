package com.neo.androidcommon.AppGlobal;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.neo.androidcommon.crash.NeverCrash;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Administrator on 2018/5/17.
 * <p>
 * <p>
 * /**
 * ,%%%%%%%%,
 * ,%%/\%%%%/\%%
 * ,%%%\c "" J/%%%
 * %.       %%%%/ o  o \%%%
 * `%%.     %%%%    _  |%%%
 * `%%     `%%%%(__Y__)%%'
 * //       ;%%%%`\-/%%%'
 * ((       /  `%%%%%%%'
 * \\    .'          |
 * \\  /       \  | |
 * \\/         ) | |
 * \         /_ | |__
 * (___________)))))))
 * <p>
 * <p>
 * _       _
 * __   _(_)_   _(_) __ _ _ __
 * \ \ / / \ \ / / |/ _` | '_ \
 * \ V /| |\ V /| | (_| | | | |
 * \_/ |_| \_/ |_|\__,_|_| |_|
 */

public class CommonApplication extends Application {

    private static Context context;

    private static Handler handler;

    private static int mainThreadId;

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();
        handler = new Handler();
        mainThreadId = android.os.Process.myTid();

        initException();

    }

    private void initException() {

        NeverCrash.init(new NeverCrash.CrashHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                Log.e("报错=>", Log.getStackTraceString(e));
            }
        });


    }


    public static Context getContext() {
        return context;
    }


    public static Handler getHandler() {
        return handler;
    }


    public static int getMainThreadId() {
        return mainThreadId;
    }


    /**
     * 获取进程号对应的进程名
     *
     * @param pid 进程号
     * @return 进程名
     */
    private static String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}
