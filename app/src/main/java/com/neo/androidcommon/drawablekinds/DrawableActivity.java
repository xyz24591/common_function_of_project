package com.neo.androidcommon.drawablekinds;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/9/11.
 */

public class DrawableActivity extends BaseActivity {
    @BindView(R.id.txt_show)
    TextView txtShow;

    @Override
    public int intiLayout() {
        return R.layout.activity_drawable;
    }

    @Override
    public void initView() {

        colorDrawable();

        bitmapDrawable();

        animationDrawable();
    }

    private void animationDrawable() {


    }

    private void bitmapDrawable() {


    }

    private void colorDrawable() {
// API 需要大于 16
        ColorDrawable colorDrawable = new ColorDrawable(0xffff2200);
        txtShow.setBackground(colorDrawable);

        Button button = new Button(this);
        button.setBackgroundColor(getResources().getColor(R.color.colorPrimary));


    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
