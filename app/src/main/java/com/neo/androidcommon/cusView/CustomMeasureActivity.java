package com.neo.androidcommon.cusView;

import android.os.Bundle;
import android.widget.ImageView;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.CommonUtils.LogUtil;
import com.neo.androidcommon.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/10/19.
 */

public class CustomMeasureActivity extends BaseActivity {
    @BindView(R.id.image_measure)
    ImageView imageMeasure;

    @Override
    public int intiLayout() {
        return R.layout.activity_custom;
    }

    @Override
    public void initView() {

        int top = imageMeasure.getTop();
        int left = imageMeasure.getLeft();

        LogUtil.d("top:" + top + ";left;" + left);

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
