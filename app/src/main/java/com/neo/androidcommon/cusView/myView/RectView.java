package com.neo.androidcommon.cusView.myView;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.neo.androidcommon.CommonUtils.LogUtil;

/**
 * Created by Administrator on 2018/10/20.
 */

public class RectView extends View {

    private Paint mPaint;

    public RectView(Context context) {
        super(context);
        initDraw(context);
    }

    public RectView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initDraw(context);
    }

    public RectView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initDraw(context);
    }

    private void initDraw(Context context) {

        mPaint = new Paint();
        mPaint.setColor(Color.BLUE);
        mPaint.setStrokeWidth(1.5f);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();

//        LogUtil.d("paddingLeft:" + paddingLeft + "paddingRight：" + paddingRight + "paddingTop:" + paddingTop + "paddingBottom:" + paddingBottom);

        int width = getWidth() - paddingLeft - paddingRight;
        int height = getHeight() - paddingTop - paddingBottom;

//        LogUtil.d("width:" + width + "height" + height);

//        int width = getWidth();
//        int height = getHeight();
        canvas.drawRect(0 + paddingLeft, paddingTop, width + paddingRight, height + paddingBottom, mPaint);

    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthSpecMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightSpecMode = MeasureSpec.getMode(heightMeasureSpec);
        int widthSpecSize = MeasureSpec.getSize(widthSpecMode);
        int heightSpecSize = MeasureSpec.getSize(heightSpecMode);

        if (widthSpecMode == MeasureSpec.AT_MOST && heightSpecMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(600, 600);
        } else if (widthMeasureSpec == MeasureSpec.AT_MOST) {
            setMeasuredDimension(600, heightSpecSize);
        } else if (heightSpecMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(widthSpecSize, 600);
        }

    }
}
