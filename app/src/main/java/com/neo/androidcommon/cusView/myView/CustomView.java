package com.neo.androidcommon.cusView.myView;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Scroller;

import com.neo.androidcommon.CommonUtils.LogUtil;

/**
 * Created by Administrator on 2018/10/19.
 */

public class CustomView extends View {

    private int lastX;
    private int lastY;
    Scroller mScroller;


    public CustomView(Context context) {
        super(context);
        initView(context);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initView(context);
    }

    private void initView(Context context) {
        if (mScroller == null) {
            mScroller = new Scroller(context);

        }

    }

    @Override
    public void computeScroll() {
        super.computeScroll();

        if (mScroller.computeScrollOffset()) {

            ((View) getParent()).scrollTo(mScroller.getCurrX(), mScroller.getCurrY());
            invalidate();
        }

    }

    public void smoothScrollTo(int destX, int destY) {

        int scrollX = getScrollX();

        int delta = destX - scrollX;
        mScroller.startScroll(scrollX, 0, delta, 0, 2000);
        invalidate();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = event.getAction();
//获取 手指触摸点 的横坐标 和 纵坐标
        int x = (int) event.getX();
        int y = (int) event.getY();

        float rawX = event.getRawX();
        float rawY = event.getRawY();

        LogUtil.d("-x->" + x + ";Y;" + y + ";rawx;" + ((int) rawX) + ";rawY;" + ((int) rawY));

        switch (action) {

            case MotionEvent.ACTION_DOWN:

                lastX = x;
                lastY = y;

                break;

            case MotionEvent.ACTION_MOVE:
//计算 移动距离
                int offsetX = x - lastX;
                int offsetY = y - lastY;
                LogUtil.d("=============================================");
                LogUtil.d("======x:" + x + ";lastX;" + lastX + ";offsetX;" + offsetX);
                LogUtil.d("=============================================");
//调用 layout 重新设置 他的 布局 第一种方式
//                layout(getLeft() + offsetX, getTop() + offsetY, getRight() + offsetX, getBottom() + offsetY);
//     第二种 方式
//                offsetLeftAndRight(offsetX);
//                offsetTopAndBottom(offsetY);
//第三种方式
//                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) getLayoutParams();
//                layoutParams.leftMargin = getLeft() + offsetX;
//                layoutParams.topMargin = getRight() + offsetY;
//                setLayoutParams(layoutParams);

                ((ViewGroup) getParent()).scrollBy(-offsetX, -offsetY);

                break;


            default:
                break;
        }


        return true;


    }
}
