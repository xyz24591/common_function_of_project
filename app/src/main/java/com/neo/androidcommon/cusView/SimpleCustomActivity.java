package com.neo.androidcommon.cusView;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;
import com.neo.androidcommon.cusView.myView.CustomView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/10/19.
 */

public class SimpleCustomActivity extends BaseActivity {
    @BindView(R.id.cus_view)
    CustomView cusView;

    @Override
    public int intiLayout() {
        return R.layout.activity_simple_cunston;
    }

    @Override
    public void initView() {

//        cusView.setAnimation(AnimationUtils.loadAnimation(this, R.anim.cus_translate));

//        ObjectAnimator.ofFloat(cusView, "translationX", 0, 300).setDuration(1000).start();
//        cusView.smoothScrollTo(-400, 0);

//        ObjectAnimator animator = ObjectAnimator.ofFloat(cusView, "translationX", 200);
//        animator.setDuration(300);
//        animator.start();

//        ObjectAnimator animator = ObjectAnimator.ofFloat(cusView, "alpha", 1.5f,300);
//        animator.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//
//            }
//        });

//        animator.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                super.onAnimationEnd(animation);
//                Toast.makeText(SimpleCustomActivity.this, "动画结束", Toast.LENGTH_SHORT).show();
//            }
//        });


        ObjectAnimator animator1 = ObjectAnimator.ofFloat(cusView, "translationX", 0.0f, 200f, 0f, 0f);

        ObjectAnimator animator2 = ObjectAnimator.ofFloat(cusView, "scaleX", 1.0f, 2.0f);

        ObjectAnimator animator3 = ObjectAnimator.ofFloat(cusView, "rotationX", 0.0f, 90.0f, 0.0f);

        AnimatorSet set = new AnimatorSet();
        set.setDuration(1000);
        set.play(animator1).with(animator2).with(animator3);
        set.start();

        initMyHorView();

    }

    private ListView lv_one;
    private ListView lv_two;

    private void initMyHorView() {

        lv_one = (ListView) this.findViewById(R.id.lv_one);
        lv_two = (ListView) this.findViewById(R.id.lv_two);
        String[] strs1 = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, strs1);
        lv_one.setAdapter(adapter1);

        String[] strs2 = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O"};
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, strs2);
        lv_two.setAdapter(adapter2);


    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
