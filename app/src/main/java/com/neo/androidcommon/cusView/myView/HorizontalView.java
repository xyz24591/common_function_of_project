package com.neo.androidcommon.cusView.myView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Scroller;

import com.neo.androidcommon.CommonUtils.LogUtil;

/**
 * Created by Administrator on 2018/10/20.
 */

public class HorizontalView extends ViewGroup {


    private int lastX;
    private int lastY;

    private int currentIndex = 0; // 当前 子元素
    private int childWidth = 0;

    private Scroller scroller;
    private VelocityTracker tracker;

    private int lastInterceptX = 0;
    private int lastInterceptY = 0;


    public HorizontalView(Context context) {
        super(context);
        init(context);
    }

    public HorizontalView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public HorizontalView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        scroller = new Scroller(context);
        tracker = VelocityTracker.obtain();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        boolean intercept = false;
        int x = (int) ev.getX();
        int y = (int) ev.getY();

        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:

                intercept = false;

                if (!scroller.isFinished()) {
                    scroller.abortAnimation();
                }

                break;

            case MotionEvent.ACTION_MOVE:

                int deltaX = x - lastInterceptX;
                int deltaY = y - lastInterceptY;
//                横着 滑动
                if ((Math.abs(deltaX) - Math.abs(deltaY)) > 0) {
                    intercept = true;
                } else {
                    intercept = false;
                }

                break;
            case MotionEvent.ACTION_UP:
                intercept = false;
                break;


            default:
                break;
        }

        lastX = x;
        lastY = y;
        lastInterceptX = x;
        lastInterceptY = y;

        LogUtil.d("-->" + intercept);

        return intercept;
    }

//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        tracker.addMovement(event);
//        int x = (int) event.getX();
//        int y = (int) event.getY();
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                if (!scroller.isFinished()) {
//                    scroller.abortAnimation();
//                }
//                break;
//            case MotionEvent.ACTION_MOVE:
//                //跟随手指滑动
//                int deltaX = x - lastX;
//                scrollBy(-deltaX, 0);
//                break;
//            //释放手指以后开始自动滑动到目标位置
//            case MotionEvent.ACTION_UP:
//                //相对于当前View滑动的距离,正为向左,负为向右
//                int distance = getScrollX() - currentIndex * childWidth;
//
//                //必须滑动的距离要大于1/2个宽度,否则不会切换到其他页面
//                if (Math.abs(distance) > childWidth / 2) {
//                    if (distance > 0) {
//                        currentIndex++;
//                    } else {
//                        currentIndex--;
//                    }
//                } else {
//                    tracker.computeCurrentVelocity(1000);
//                    float xV = tracker.getXVelocity();
//                    if (Math.abs(xV) > 50) {
//                        if (xV > 0) {
//                            currentIndex--;
//                        } else {
//                            currentIndex++;
//                        }
//                    }
//                }
//                currentIndex = currentIndex < 0 ? 0 : currentIndex > getChildCount() - 1 ? getChildCount() - 1 : currentIndex;
//                smoothScrollTo(currentIndex * childWidth, 0);
//                tracker.clear();
//                break;
//            default:
//                break;
//        }
//        lastX = x;
//        lastY = y;
//        return true;
//    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        tracker.addMovement(event);
        int x = (int) event.getX();
        int y = (int) event.getY();

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:

                if (!scroller.isFinished()) {
                    scroller.abortAnimation();
                }

                break;

            case MotionEvent.ACTION_MOVE:
                int deltaX = x - lastX;
                scrollBy(-deltaX, 0);
                break;

            case MotionEvent.ACTION_UP:

//                LogUtil.d("-getScrollX()::>" + getScrollX());

                int distance = getScrollX() - currentIndex * childWidth;

                if (Math.abs(distance) > childWidth / 2) {

                    if (distance > 0) {
                        currentIndex++;
                    } else {
                        currentIndex--;
                    }


                } else {

                    tracker.computeCurrentVelocity(1000);
                    float xV = tracker.getXVelocity();
//切换到 上个页面
                    if (Math.abs(xV) > 50) {

                        if (xV > 0) {
                            currentIndex--;
                            //切换到 下个页面
                        } else {
                            currentIndex++;
                        }
                    }

                }

                currentIndex = currentIndex < 0 ? 0 : currentIndex > getChildCount() - 1 ? getChildCount() - 1 : currentIndex;
                smoothScrollTo(currentIndex * childWidth, 0);
                tracker.clear();
                break;

            default:
                break;
        }

        lastX = x;
        lastY = y;

        return true;


    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {


        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);

        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        measureChildren(widthMeasureSpec, heightMeasureSpec);
//        如果 宽和高 都是 AT_MOST 则 宽度设置为 所有 子元素的宽高 高度设置为第一个子元素的 高
        if (widthMode == MeasureSpec.AT_MOST && heightMode == MeasureSpec.AT_MOST) {

            View childOne = getChildAt(0);
            int childWidth = childOne.getMeasuredWidth();
            int childHeight = childOne.getMeasuredHeight();

            setMeasuredDimension(childWidth * getChildCount(), childHeight);

        } else if (widthMode == MeasureSpec.AT_MOST) {
            int childWidth = getChildAt(0).getMeasuredWidth();
            setMeasuredDimension(childWidth * getChildCount(), heightSize);

        } else if (heightMode == MeasureSpec.AT_MOST) {

            int childHeight = getChildAt(0).getMeasuredHeight();
            setMeasuredDimension(widthSize, childHeight);
        }


        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }


    @Override
    public void computeScroll() {
        super.computeScroll();

        if (scroller.computeScrollOffset()) {
            scrollTo(scroller.getCurrX(), scroller.getCurrY());
            postInvalidate();

        }

    }

    //弹性滑动到 指定位置
    public void smoothScrollTo(int destX, int destY) {

        scroller.startScroll(getScrollX(), getScrollY(), destX - getScrollX(), destY - getScrollY(), 1000);
        invalidate();

    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        int childCount = getChildCount();
        int left = 0;
        View child;

        for (int i = 0; i < childCount; i++) {

            child = getChildAt(i);

            if (child.getVisibility() != View.GONE) {

                int width = child.getMeasuredHeight();

                childWidth = width;
                child.layout(left, 0, left + width, child.getMeasuredHeight());
                left += width;

            }

        }

    }


}
