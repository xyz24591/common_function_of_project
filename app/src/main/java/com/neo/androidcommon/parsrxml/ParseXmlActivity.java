package com.neo.androidcommon.parsrxml;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;
import com.neo.androidcommon.parsrxml.entity.Person;
import com.neo.androidcommon.parsrxml.helper.DomHelper;
import com.neo.androidcommon.parsrxml.helper.PullHelper;
import com.neo.androidcommon.parsrxml.helper.SaxHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/9/10.
 */

public class ParseXmlActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.btnsax)
    Button btnsax;
    @BindView(R.id.btndom)
    Button btndom;
    @BindView(R.id.btnpullread)
    Button btnpullread;
    @BindView(R.id.btnpullwrite)
    Button btnpullwrite;
    @BindView(R.id.list)
    ListView list;
    @BindView(R.id.LinearLayout1)
    LinearLayout LinearLayout1;

    private ArrayList<Person> persons;
    private ArrayAdapter<Person> mAdapter;

    @Override
    public int intiLayout() {
        return R.layout.activity_parse_xml;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

        btnsax.setOnClickListener(this);
        btndom.setOnClickListener(this);
        btnpullread.setOnClickListener(this);
        btnpullwrite.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnsax:
                try {
                    persons = readxmlForSAX();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mAdapter = new ArrayAdapter<Person>(ParseXmlActivity.this,
                        android.R.layout.simple_expandable_list_item_1, persons);
                list.setAdapter(mAdapter);
                break;
            case R.id.btndom:
//                DomHelper ds = new DomHelper();
                persons = DomHelper.queryXML(getApplicationContext());
                mAdapter = new ArrayAdapter<Person>(ParseXmlActivity.this, android.R.layout.simple_expandable_list_item_1, persons);
                list.setAdapter(mAdapter);
                break;
            case R.id.btnpullread:
                //获取文件资源建立输入流对象
                try {
                    InputStream is = getAssets().open("person3.xml");
                    persons = PullHelper.getPersons(is);
                    if (persons.equals(null)) {
                        Toast.makeText(getApplicationContext(), "呵呵", Toast.LENGTH_SHORT).show();
                    }
                    for (Person p1 : persons) {
                        Log.i("逗比", p1.toString());
                    }
                    mAdapter = new ArrayAdapter<Person>(ParseXmlActivity.this, android.R.layout.simple_expandable_list_item_1, persons);
                    list.setAdapter(mAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btnpullwrite:
                Context context = getApplicationContext();
                List<Person> persons = new ArrayList<Person>();
                persons.add(new Person(21, "逗比1", 70));
                persons.add(new Person(31, "逗比2", 50));
                persons.add(new Person(11, "逗比3", 30));
                File xmlFile = new File(context.getFilesDir(), "jay.xml");
                FileOutputStream fos;
                try {
                    fos = new FileOutputStream(xmlFile);
                    PullHelper.save(persons, fos);
                    Toast.makeText(context, "文件写入完毕", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            default:
                break;


        }


    }


    private ArrayList<Person> readxmlForSAX() throws Exception {
        //获取文件资源建立输入流对象
        InputStream is = getAssets().open("person1.xml");
        //①创建XML解析处理器
        SaxHelper ss = new SaxHelper();
        //②得到SAX解析工厂
        SAXParserFactory factory = SAXParserFactory.newInstance();
        //③创建SAX解析器
        SAXParser parser = factory.newSAXParser();
        //④将xml解析处理器分配给解析器,对文档进行解析,将事件发送给处理器
        parser.parse(is, ss);
        is.close();
        return ss.getPersons();
    }


}
