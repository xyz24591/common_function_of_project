package com.neo.androidcommon.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.neo.androidcommon.Interfacepro.OnItemClickListener;
import com.neo.androidcommon.R;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018/9/28.
 */

public class RecAdapter extends RecyclerView.Adapter<RecAdapter.RecViewHolder> {

    private ArrayList<String> datas;
    private Context context;


    public RecAdapter(ArrayList<String> datas, Context context) {
        this.datas = datas;
        this.context = context;
    }

    @Override
    public RecViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_recy, parent, false);

        return new RecViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecViewHolder holder, int position) {

        final String itemData = datas.get(position).toString().trim();

        holder.itemTv.setText(itemData);

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = holder.getLayoutPosition();
                if (itemClickListener != null) {
                    itemClickListener.onItemClick(holder.rootView,pos);
                }

            }
        });

        holder.rootView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                int pos = holder.getLayoutPosition();
                if (itemClickListener != null) {
                    itemClickListener.onItemLongClick(holder.rootView,pos);
                }

                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    public class RecViewHolder extends RecyclerView.ViewHolder {

        private final TextView itemTv;
        private final LinearLayout rootView;

        public RecViewHolder(View itemView) {
            super(itemView);

            itemTv = itemView.findViewById(R.id.item_tv);
            rootView = itemView.findViewById(R.id.rootView);
        }
    }

    OnItemClickListener itemClickListener;

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {

        this.itemClickListener = itemClickListener;

    }

}
