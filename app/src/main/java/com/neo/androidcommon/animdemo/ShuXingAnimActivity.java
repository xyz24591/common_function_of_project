package com.neo.androidcommon.animdemo;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import butterknife.BindView;

/**
 * Created by Administrator on 2018/9/12.
 */

public class ShuXingAnimActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.btn_one)
    Button btnOne;
    @BindView(R.id.btn_two)
    Button btnTwo;
    @BindView(R.id.btn_three)
    Button btnThree;
    @BindView(R.id.btn_four)
    Button btnFour;
    @BindView(R.id.img_babi)
    ImageView imgBabi;
    @BindView(R.id.ly_root)
    LinearLayout lyRoot;

    private int width;
    private int height;

    @Override
    public int intiLayout() {
        return R.layout.activity_shuxing;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {
        btnOne.setOnClickListener(this);
        btnTwo.setOnClickListener(this);
        btnThree.setOnClickListener(this);
        btnFour.setOnClickListener(this);
        imgBabi.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_one:
                lineAnimator();
                break;
            case R.id.btn_two:
                scaleAnimator();
                break;
            case R.id.btn_three:
                raAnimator();
                break;
            case R.id.btn_four:
                circleAnimator();
                break;
            case R.id.img_babi:
                Toast.makeText(this, "咻咻咻~~~~", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }

    }

    //定义一个修改ImageView位置的方法
    private void moveView(View view, int rawX, int rawY) {
        int left = rawX - imgBabi.getWidth() / 2;
        int top = rawY - imgBabi.getHeight();
        int width = left + view.getWidth();
        int height = top + view.getHeight();
        view.layout(left, top, width, height);
    }

    //定义属性动画的方法：

    //按轨迹方程来运动
    private void lineAnimator() {

        final int width = lyRoot.getWidth();
        int height = lyRoot.getHeight();

        ValueAnimator xValue = ValueAnimator.ofInt(height, 0, height / 4, height / 2, height / 4 * 3, height);
        xValue.setDuration(3000);
        xValue.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // 轨迹方程 x = width / 2

                int y = (Integer) animation.getAnimatedValue();
                int x = width / 2;
                moveView(imgBabi, x, y);
            }
        });
        xValue.setInterpolator(new LinearInterpolator());
        xValue.start();
    }

    private void scaleAnimator() {
        //用两个组合动画~
        float scale = 0.5f;
        AnimatorSet scaleSet = new AnimatorSet();
        ValueAnimator valueAnimatorSmall = ValueAnimator.ofFloat(1.0f, scale);
        valueAnimatorSmall.setDuration(500);

        ValueAnimator valueAnimatorLarge = ValueAnimator.ofFloat(scale, 1.0f);
        valueAnimatorLarge.setDuration(500);

        valueAnimatorSmall.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float scale = (Float) animation.getAnimatedValue();
                imgBabi.setScaleX(scale);
                imgBabi.setScaleY(scale);
            }
        });

        valueAnimatorLarge.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {

                float scale = (Float) animation.getAnimatedValue();
                imgBabi.setScaleX(scale);
                imgBabi.setScaleY(scale);

            }
        });
        scaleSet.play(valueAnimatorLarge).after(valueAnimatorSmall);
        scaleSet.start();

        //其实可以一个就搞定的
//        ValueAnimator vValue = ValueAnimator.ofFloat(1.0f, 0.6f, 1.2f, 1.0f, 0.6f, 1.2f, 1.0f);
//        vValue.setDuration(1000L);
//        vValue.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator animation) {
//                float scale = (Float) animation.getAnimatedValue();
//                img_babi.setScaleX(scale);
//                img_babi.setScaleY(scale);
//            }
//        });
//        vValue.setInterpolator(new LinearInterpolator());
//        vValue.start();

    }

    private void raAnimator() {

        ValueAnimator rValue = ValueAnimator.ofInt(0, 360);
        rValue.setDuration(1000);
        rValue.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {

                int rotateValue = (Integer) animation.getAnimatedValue();
                imgBabi.setRotation(rotateValue);

                float fractionValue = animation.getAnimatedFraction();
                imgBabi.setAlpha(fractionValue);
            }
        });
        rValue.setInterpolator(new DecelerateInterpolator());
        rValue.start();

    }

    private void circleAnimator() {

        width = lyRoot.getWidth();
        height = lyRoot.getHeight();

        final int r = width / 4;

        ValueAnimator tValue = ValueAnimator.ofFloat(0, (float) (2.0f * Math.PI));
        tValue.setDuration(1000);
        tValue.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
// 圆的参数方程 x = r * sin(t) y = r * cos(t)
                float t = (Float) animation.getAnimatedValue();
                int x = (int) (r * Math.sin(t) + width / 2);
                int y = (int) (r * Math.cos(t) + height / 2);
                moveView(imgBabi, x, y);

            }
        });
        tValue.setInterpolator(new DecelerateInterpolator());
        tValue.start();

    }



}
