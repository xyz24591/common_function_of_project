package com.neo.androidcommon.animdemo;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/9/11.
 */

public class BuJianAnimActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.btn_alpha)
    Button btnAlpha;
    @BindView(R.id.btn_scale)
    Button btnScale;
    @BindView(R.id.btn_tran)
    Button btnTran;
    @BindView(R.id.btn_rotate)
    Button btnRotate;
    @BindView(R.id.btn_set)
    Button btnSet;
    @BindView(R.id.img_show)
    ImageView imgShow;
    @BindView(R.id.start_ctrl)
    LinearLayout startCtrl;
    private Animation animation = null;

    @Override
    public int intiLayout() {
        return R.layout.activity_bujian_anim;
    }

    @Override
    public void initView() {

        //设置动画，从自身位置的最下端向上滑动了自身的高度，持续时间为500ms

        final TranslateAnimation translateAnimation = new TranslateAnimation(TranslateAnimation.RELATIVE_TO_SELF, 0, TranslateAnimation.RELATIVE_TO_SELF, 0,
                TranslateAnimation.RELATIVE_TO_SELF, 1, TranslateAnimation.RELATIVE_TO_SELF, 0);
        translateAnimation.setDuration(500);

        startCtrl.postDelayed(new Runnable() {
            @Override
            public void run() {

                startCtrl.setVisibility(View.VISIBLE);
                startCtrl.startAnimation(translateAnimation);

            }
        }, 1000);

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {
        btnAlpha.setOnClickListener(this);
        btnScale.setOnClickListener(this);
        btnTran.setOnClickListener(this);
        btnRotate.setOnClickListener(this);
        btnSet.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_alpha:

                animation = AnimationUtils.loadAnimation(this, R.anim.anim_alpha);
                imgShow.startAnimation(animation);
                break;

            case R.id.btn_scale:
                animation = AnimationUtils.loadAnimation(this,
                        R.anim.anim_scale);
                imgShow.startAnimation(animation);
                break;
            case R.id.btn_tran:
                animation = AnimationUtils.loadAnimation(this,
                        R.anim.anim_translate);
                imgShow.startAnimation(animation);
                break;
            case R.id.btn_rotate:
                animation = AnimationUtils.loadAnimation(this,
                        R.anim.anim_rotate);
                imgShow.startAnimation(animation);
                break;
            case R.id.btn_set:
                animation = AnimationUtils.loadAnimation(this,
                        R.anim.anim_set);
                imgShow.startAnimation(animation);
                break;
            default:
                break;

        }


    }
}
