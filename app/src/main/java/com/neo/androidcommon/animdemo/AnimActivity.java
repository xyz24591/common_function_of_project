package com.neo.androidcommon.animdemo;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/9/11.
 */

public class AnimActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.btn_start)
    Button btnStart;
    @BindView(R.id.btn_stop)
    Button btnStop;
    @BindView(R.id.img_show)
    ImageView imgShow;
    @BindView(R.id.f_iv)
    FrameView fIv;
    private AnimationDrawable anim;

    @Override
    public int intiLayout() {
        return R.layout.activity_anim;
    }

    @Override
    public void initView() {
        btnStart.setOnClickListener(this);
        btnStop.setOnClickListener(this);
        anim = (AnimationDrawable) imgShow.getBackground();

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_start:
                anim.start();
                break;
            case R.id.btn_stop:
                anim.stop();
                break;
            default:
                break;
        }

    }
}
