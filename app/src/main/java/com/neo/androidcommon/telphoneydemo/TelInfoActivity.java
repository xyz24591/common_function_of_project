package com.neo.androidcommon.telphoneydemo;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.widget.TextView;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

/**
 * Created by Administrator on 2018/9/12.
 */

public class TelInfoActivity extends BaseActivity {

    private TextView tv_phone1;
    private TextView tv_phone2;
    private TextView tv_phone3;
    private TextView tv_phone4;
    private TextView tv_phone5;
    private TextView tv_phone6;
    private TextView tv_phone7;
    private TextView tv_phone8;
    private TextView tv_phone9;
    private TextView tv_phone10;
    private TelephonyManager tManager;
    private String[]                                                                                                                                                                                                                                                                                                                                                                                                        phoneType = {"未知", "2G", "3G", "4G"};
    private String[] simState = {"状态未知", "无SIM卡", "被PIN加锁", "被PUK加锁",
            "被NetWork PIN加锁", "已准备好"};

    @Override
    public int intiLayout() {
        return R.layout.activity_tel_info;
    }

    @Override
    public void initView() {

        tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        tv_phone1 = (TextView) findViewById(R.id.tv_phone1);
        tv_phone2 = (TextView) findViewById(R.id.tv_phone2);
        tv_phone3 = (TextView) findViewById(R.id.tv_phone3);
        tv_phone4 = (TextView) findViewById(R.id.tv_phone4);
        tv_phone5 = (TextView) findViewById(R.id.tv_phone5);
        tv_phone6 = (TextView) findViewById(R.id.tv_phone6);
        tv_phone7 = (TextView) findViewById(R.id.tv_phone7);
        tv_phone8 = (TextView) findViewById(R.id.tv_phone8);
        tv_phone9 = (TextView) findViewById(R.id.tv_phone9);
        tv_phone10 = (TextView) findViewById(R.id.tv_phone10);

        initSingalStrength();

    }

    private void initSingalStrength() {

        MyPhoneStateListener myPhoneStateListener = new MyPhoneStateListener();

        tManager.listen(myPhoneStateListener, 290);

    }

    private class MyPhoneStateListener extends PhoneStateListener {
        private int asu = 0, lastSignal = 0;

        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            asu = signalStrength.getGsmSignalStrength();
            lastSignal = -113 + 2 * asu;
            tv_phone10.setText("当前手机的信号强度：" + lastSignal + " dBm");

            super.onSignalStrengthsChanged(signalStrength);
        }
    }


    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

        tv_phone1.setText("设备编号:" + tManager.getDeviceId());
        tv_phone2.setText("软件版本:" + (tManager.getDeviceSoftwareVersion() != null ?
                tManager.getDeviceSoftwareVersion() : "未知"));
        tv_phone3.setText("运营商代号:" + tManager.getNetworkOperator());
        tv_phone4.setText("运营商名称:" + tManager.getNetworkOperatorName());
        tv_phone5.setText("网络类型：" + phoneType[tManager.getPhoneType()]);
        tv_phone6.setText("设备当前位置:" + (tManager.getCellLocation() != null ? tManager
                .getCellLocation().toString() : "未知位置"));

        tv_phone7.setText("SIM卡的国别：" + tManager.getSimCountryIso());
        tv_phone8.setText("SIM卡序列号：" + tManager.getSimSerialNumber());
        tv_phone9.setText("SIM卡状态：" + simState[tManager.getSimState()]);

    }

}
