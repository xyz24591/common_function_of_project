package com.neo.androidcommon.telphoneydemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/9/12.
 */

public class TelephoneyMainActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.getInfo)
    Button getInfo;
    @BindView(R.id.incallInfo)
    Button incallInfo;

    @Override
    public int intiLayout() {
        return R.layout.activity_tel_main;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {
        getInfo.setOnClickListener(this);
        incallInfo.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.getInfo:

                startActivity(new Intent(TelephoneyMainActivity.this, TelInfoActivity.class));

                break;
            case R.id.incallInfo:

                startActivity(new Intent(TelephoneyMainActivity.this, TelphoneyCallActivity.class));

                break;

            default:
                break;

        }

    }
}
