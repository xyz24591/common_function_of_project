package com.neo.androidcommon.telphoneydemo;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.CommonUtils.LogUtil;
import com.neo.androidcommon.R;

import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Date;

/**
 * Created by Administrator on 2018/9/12.
 */

public class TelphoneyCallActivity extends BaseActivity {
    TelephonyManager tManager;

    @Override
    public int intiLayout() {
        return R.layout.activity_telphony_call;
    }

    @Override
    public void initView() {

        initPhone();

    }

    private void initPhone() {
        tManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);

        // 创建一个通话状态监听器
        PhoneStateListener listener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                super.onCallStateChanged(state, incomingNumber);
                switch (state) {

                    case TelephonyManager.CALL_STATE_IDLE:
                        LogUtil.i("空闲中...");
                        break;
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                        LogUtil.i("挂断电话");
                        break;
                    case TelephonyManager.CALL_STATE_RINGING:

                        OutputStream outputStream = null;

                        try {
                            outputStream = openFileOutput("phoneList", MODE_APPEND);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        PrintStream ps = new PrintStream(outputStream);
                        ps.println(new Date() + "来电：" + incomingNumber);
                        ps.close();
                        Toast.makeText(TelphoneyCallActivity.this, "电话"+incomingNumber, Toast.LENGTH_SHORT).show();
                        break;


                    default:
                        break;

                }


            }
        };
tManager.listen(listener,PhoneStateListener.LISTEN_CALL_STATE);
    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }
}
