package com.neo.androidcommon.parseJson;

import android.view.View;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/9/10.
 */

public class ParseJsonActivity extends BaseActivity {

    private String json = "[\n" +
            "    { \"id\":\"1\",\"name\":\"基神\",\"age\":\"18\" },\n" +
            "    { \"id\":\"2\",\"name\":\"B神\",\"age\":\"18\"  },\n" +
            "    { \"id\":\"3\",\"name\":\"曹神\",\"age\":\"18\" }\n" +
            "]";

    @Override
    public int intiLayout() {
        return R.layout.activity_json_parse;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }

    public void parseEasyJson(View view) {

        List<Person> persons = new ArrayList<>();

        try {
            JSONArray jsonArray = new JSONArray(json);

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                Person person = new Person();
                person.setId(i + "");
                person.setName(jsonObject.getString("name"));
                person.setAge(jsonObject.getString("age"));
                persons.add(person);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
