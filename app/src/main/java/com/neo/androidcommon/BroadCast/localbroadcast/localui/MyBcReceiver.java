package com.neo.androidcommon.BroadCast.localbroadcast.localui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.Window;
import android.view.WindowManager;

import com.neo.androidcommon.BroadCast.localbroadcast.ActivityCollector;
import com.neo.androidcommon.BroadCast.localbroadcast.LoginActivity;

/**
 * Created by Administrator on 2018/9/3.
 */

public class MyBcReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(final Context context, Intent intent) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("警告：");
        builder.setMessage("您的账号在别处登录,请重新登录");
        builder.setCancelable(false);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCollector.finishAll();
                Intent intent = new Intent(context, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_DIALOG);
        alertDialog.show();

    }
}
