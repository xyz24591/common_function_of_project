package com.neo.androidcommon.BroadCast.localbroadcast;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;

import com.neo.androidcommon.BroadCast.MyBRReceiver;
import com.neo.androidcommon.R;

/**
 * Created by Administrator on 2018/9/3.
 */

public class MainBrActivity extends LocalBaseActivity {
    private IntentFilter intentFilter;
    private LocalBroadcastManager localBroadcastManager;
    private MyBRReceiver myBRReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_local_main);

        initBroadcast();
    }

    private void initBroadcast() {

        localBroadcastManager = LocalBroadcastManager.getInstance(this);

        myBRReceiver = new MyBRReceiver();
        intentFilter = new IntentFilter();
        intentFilter.addAction("com.jay.mybcreceiver.LOGIN_OTHER");
        localBroadcastManager.registerReceiver(myBRReceiver, intentFilter);
    }


    public void loginOther(View view) {
        Intent intent = new Intent("com.jay.mybcreceiver.LOGIN_OTHER");
        localBroadcastManager.sendBroadcast(intent);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        localBroadcastManager.unregisterReceiver(myBRReceiver);
    }
}
