package com.neo.androidcommon;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.neo.androidcommon.CommonUtils.UIUtils;
import com.neo.androidcommon.Entity.DemoDetails;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/5/5.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeAdapterViewHolder> {

    List<DemoDetails> demoDetailses;


    private Context context;

    public HomeAdapter(Context context, List<DemoDetails> demoDetailses) {
        this.demoDetailses = demoDetailses;
        this.context = context;
    }


    @Override
    public HomeAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.home_item_layout, null);

        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(layoutParams);
        return new HomeAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeAdapterViewHolder holder, final int position) {

        DemoDetails demoDetails = demoDetailses.get(position);
//        holder.homeTitle.setText(String.valueOf(demoDetails.getTitleId()));
//        holder.homeTitleDesc.setText(String.valueOf(demoDetails.getDescriptionId()));

        holder.homeTitle.setText(UIUtils.getString(demoDetails.getTitleId()));
        holder.homeTitleDesc.setText(UIUtils.getString(demoDetails.getDescriptionId()));

        holder.itemRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (recyclerViewClick != null) {

                    recyclerViewClick.RecyclerViewClick(position);

                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return demoDetailses == null ? 0 : demoDetailses.size();
    }

    public class HomeAdapterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.home_title)
        TextView homeTitle;
        @BindView(R.id.home_title_desc)
        TextView homeTitleDesc;
        @BindView(R.id.item_root)
        LinearLayout itemRoot;

        public HomeAdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }

    OnRecyclerViewClick recyclerViewClick;

    public void setOnRecyclerViewClickListener(OnRecyclerViewClick recyclerViewClick) {
        this.recyclerViewClick = recyclerViewClick;
    }

    public interface OnRecyclerViewClick {
        void RecyclerViewClick(int position);
    }

}
