package com.neo.androidcommon.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Administrator on 2018/7/31.
 */

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("------AlarmReceiver---", "onReceive: ");
        Intent i = new Intent(context, LongRunningService.class);
        context.startService(i);

    }
}
