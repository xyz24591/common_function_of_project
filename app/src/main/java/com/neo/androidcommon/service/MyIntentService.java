package com.neo.androidcommon.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by Administrator on 2018/7/30.
 */

public class MyIntentService extends IntentService {

    private final String TAG = "MyIntentService";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public MyIntentService(String name) {
        super(name);
    }

    public MyIntentService() {
        super("test");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        //Intent是从Activity发过来的，携带识别参数，根据参数不同执行不同的任务

        String action = intent.getExtras().getString("param");

        if (action.equals("s1")) Log.i(TAG, "启动service1");
        else if (action.equals("s2")) Log.i(TAG, "启动service2");
        else if (action.equals("s3")) Log.i(TAG, "启动service3");

        SystemClock.sleep(2000);

    }

    //重写其他方法,用于查看方法的调用顺序
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "onBind");
        return super.onBind(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate");
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {

        Log.i(TAG, "onStartCommand");

        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void setIntentRedelivery(boolean enabled) {
        super.setIntentRedelivery(enabled);
        Log.i(TAG, "setIntentRedelivery: ");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy: ");
    }
}
