package com.neo.androidcommon.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.neo.androidcommon.MainActivity;
import com.neo.androidcommon.R;

/**
 * Created by Administrator on 2018/9/13.
 */

public class WeatherService extends Service {
    private static final int NOTIFY_ID = 123;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        showNotification();
    }

    /**
     * 通知栏 显示天气
     */
    private void showNotification() {

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.and_icon)
                .setContentTitle("天气标题")
                .setContentText("内容");
//创建 通知栏 被点击时 出发的 Intent
        Intent resultIntent = new Intent(this, MainActivity.class);
//创建 任务栈 builder
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//        构建通知
        Notification notification = mBuilder.build();
//        显示通知
        manager.notify(NOTIFY_ID, notification);
//启动为 前台服务
        startForeground(NOTIFY_ID, notification);

    }
}
