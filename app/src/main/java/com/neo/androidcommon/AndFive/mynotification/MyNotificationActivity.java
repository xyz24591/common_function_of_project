package com.neo.androidcommon.AndFive.mynotification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.neo.androidcommon.BaseActivity;
import com.neo.androidcommon.MainActivity;
import com.neo.androidcommon.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/10/8.
 */

public class MyNotificationActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.tv_nomal)
    TextView tvNomal;
    @BindView(R.id.tv_fold)
    TextView tvFold;
    @BindView(R.id.tv_hang)
    TextView tvHang;
    @BindView(R.id.rb_public)
    RadioButton rbPublic;
    @BindView(R.id.rb_private)
    RadioButton rbPrivate;
    @BindView(R.id.rb_secret)
    RadioButton rbSecret;
    @BindView(R.id.rg_all)
    RadioGroup rgAll;
    private NotificationManager notificationManager;

    @Override
    public int intiLayout() {
        return R.layout.activity_my_notification;
    }

    @Override
    public void initView() {
        tvNomal.setOnClickListener(this);
        tvFold.setOnClickListener(this);
        tvHang.setOnClickListener(this);

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initListener() {

    }


    private void myNo() {

        Notification.Builder builder = new Notification.Builder(this);
        Intent intent = new Intent(this, MainActivity.class);
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);
        builder.setContentTitle("标题");
        builder.setContentText("内容");

        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.view_fold);
        Notification notification = builder.build();
        notification.bigContentView = remoteViews;

        manager.notify(0, notification);

    }


    private String url = "http://blog.csdn.net/itachi85/";

    private void sendNormalNotification() {
        Notification.Builder builder = new Notification.Builder(this);
        Intent mIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, mIntent, 0);
        builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(R.mipmap.ic_launcher_round);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        builder.setAutoCancel(true);
        builder.setContentTitle("普通通知");
        selectNotificationLevel(builder);
        notificationManager.notify(0, builder.build());

    }


    private void selectNotificationLevel(Notification.Builder builder) {

        switch (rgAll.getCheckedRadioButtonId()) {

            case R.id.rb_public:

                builder.setVisibility(Notification.VISIBILITY_PUBLIC);
                builder.setContentText("public");

                break;

            case R.id.rb_private:

                builder.setVisibility(Notification.VISIBILITY_PRIVATE);
                builder.setContentText("private");

                break;

            case R.id.rb_secret:

                builder.setVisibility(Notification.VISIBILITY_SECRET);

                builder.setContentText("secret");

                break;

            default:

                builder.setVisibility(Notification.VISIBILITY_PUBLIC);
                builder.setContentText("public");
                break;

        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_nomal:
                sendNormalNotification();

                break;
            case R.id.tv_fold:
                sendFoldNotification();
                break;

            case R.id.tv_hang:
                sendHangNotification();
                break;
            default:
                break;
        }
    }

    private void sendHangNotification() {
        Notification.Builder builder = new Notification.Builder(this);

        Intent mIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, mIntent, 0);
        builder.setContentIntent(pendingIntent);

        builder.setContentTitle("悬挂式通知");

        builder.setSmallIcon(R.mipmap.ic_launcher_round);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        builder.setAutoCancel(true);
        selectNotificationLevel(builder);

        builder.setAutoCancel(true);

//        设置点击事件 点击跳转
        Intent hangIntent = new Intent();
        hangIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        hangIntent.setClass(this, MyNotificationActivity.class);
        //如果描述的PendingIntent已经存在，则在产生新的Intent之前会先取消掉当前的
        PendingIntent hangPendIntent = PendingIntent.getActivity(this, 0, hangIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setFullScreenIntent(hangPendIntent, true);
        notificationManager.notify(2, builder.build());


    }

    private void sendFoldNotification() {

        Notification.Builder builder = new Notification.Builder(this);
        Intent mIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, mIntent, 0);
        builder.setContentIntent(pendingIntent);
        builder.setContentTitle("折叠通知");
        builder.setSmallIcon(R.mipmap.ic_launcher_round);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        builder.setAutoCancel(true);
        selectNotificationLevel(builder);
//用RemoteViews来创建自定义Notification视图
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.view_fold);
        Notification notification = builder.build();
        //指定展开时的视图
        notification.bigContentView = remoteViews;
        notificationManager.notify(1, notification);

    }

}
