package com.neo.androidcommon;


import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.neo.androidcommon.Activity.AsyncTaskActivity;
import com.neo.androidcommon.Activity.BroadCastActivity;
import com.neo.androidcommon.Activity.ConfigInfoActivity;
import com.neo.androidcommon.Activity.EditTextActivity;
import com.neo.androidcommon.Activity.GestureActivity;
import com.neo.androidcommon.Activity.GestureViewActivity;
import com.neo.androidcommon.Activity.GetstureViewGetActivity;
import com.neo.androidcommon.Activity.RecyclerViewActivity;
import com.neo.androidcommon.Activity.ServiceActivity;
import com.neo.androidcommon.AndFive.mycardview.CardViewActivity;
import com.neo.androidcommon.AndFive.mynotification.MyNotificationActivity;
import com.neo.androidcommon.CommonUtils.LogUtil;
import com.neo.androidcommon.Entity.DemoDetails;
import com.neo.androidcommon.MaterialDesignDemo.MDCoorActivity;
import com.neo.androidcommon.MaterialDesignDemo.MDCoorDetailActivity;
import com.neo.androidcommon.MaterialDesignDemo.MDMainactivity;
import com.neo.androidcommon.MaterialDesignDemo.MDTabLayoutActivity;
import com.neo.androidcommon.Vibratordemo.VibratorActivity;
import com.neo.androidcommon.animdemo.AnimActivity;
import com.neo.androidcommon.animdemo.BuJianAnimActivity;
import com.neo.androidcommon.animdemo.ShuXingAnimActivity;
import com.neo.androidcommon.bitmapdemo.BitmaoActivity;
import com.neo.androidcommon.bitmapdemo.SimplePaintActivity;
import com.neo.androidcommon.bitmapdemo.gamepaint.GamePaintActivity;
import com.neo.androidcommon.contentprovider.MainContentActivity;
import com.neo.androidcommon.cusView.CustomMeasureActivity;
import com.neo.androidcommon.cusView.SimpleCustomActivity;
import com.neo.androidcommon.exercisedemo.DemoMainActivity;
import com.neo.androidcommon.exercisedemo.drysiterrd.DemoMain3Activity;
import com.neo.androidcommon.exercisedemo.drysitetnd.DemoMain2Activity;
import com.neo.androidcommon.filewrite.FileWriteActivity;
import com.neo.androidcommon.parseJson.ParseJsonActivity;
import com.neo.androidcommon.parsrxml.ParseXmlActivity;
import com.neo.androidcommon.retrofitDemo.RetrifitMainActivity;
import com.neo.androidcommon.sensor.SensorActivity;
import com.neo.androidcommon.sharedpreference.SharedPreferenceActivity;
import com.neo.androidcommon.sqlitedb.SQLiteActivity;
import com.neo.androidcommon.telphoneydemo.TelephoneyMainActivity;
import com.neo.androidcommon.webview.WebViewActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


public class MainActivity extends BaseActivity {

    //    @BindView(R.id.home_list)
    RecyclerView homeList;
    List<DemoDetails> demoDetailses = new ArrayList<>();

    private Context context;
    private HomeAdapter homeAdapter;

    @Override
    public int intiLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void initView() {
        context = MainActivity.this;
        homeList = (RecyclerView) findViewById(R.id.home_list);

        homeList.setHasFixedSize(true);
        homeList.setLayoutManager(new LinearLayoutManager(this));

        initNotification();

    }

    private void initNotification() {


//        Notification.Builder builder = new Notification.Builder(this);
//        builder.setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0));
//        builder.setAutoCancel(false);
//        builder.setSmallIcon(R.mipmap.ic_launcher);
//        builder.setTicker("本地通知");
//        builder.setContentTitle("通知标题");
//        builder.setContentText("通知内容");
    }

    @Override
    public void initData() {

        demoDetailses.add(new DemoDetails(R.string.common_view, R.string.common_view_detail, null));

        demoDetailses.add(new DemoDetails(R.string.textview_view, R.string.textview_view_detail, TextViewActivity.class));
        demoDetailses.add(new DemoDetails(R.string.textview_view, R.string.textview_view_detail, EditTextActivity.class));
        demoDetailses.add(new DemoDetails(R.string.config_view, R.string.config_view_detail, ConfigInfoActivity.class));
        demoDetailses.add(new DemoDetails(R.string.async_view, R.string.async_view_detail, AsyncTaskActivity.class));
        demoDetailses.add(new DemoDetails(R.string.async_guest_huadong, R.string.async_guest_huadong_detail, GestureActivity.class));
        demoDetailses.add(new DemoDetails(R.string.async_guest_save, R.string.async_guest_save_detail, GestureViewActivity.class));
        demoDetailses.add(new DemoDetails(R.string.async_guest, R.string.async_guest_detail, GetstureViewGetActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_service, R.string.view_service_detail, ServiceActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_broadcast, R.string.view_broadcast_detail, BroadCastActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_contentprovider, R.string.view_contentprovider_detail, MainContentActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_file, R.string.view_file_detail, FileWriteActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_sp, R.string.view_sp_detail, SharedPreferenceActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_sqlite, R.string.view_sqlite_detail, SQLiteActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_xml, R.string.view_xml_detail, ParseXmlActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_json, R.string.view_json_detail, ParseJsonActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_webview, R.string.view_webview_detail, WebViewActivity.class));
//        demoDetailses.add(new DemoDetails(R.string.view_javajs, R.string.view_javajs_detail, WebViewActivity.class));
//        demoDetailses.add(new DemoDetails(R.string.view_javajs, R.string.view_javajs_detail, DrawableActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_bitmap, R.string.view_bitmap_detail, BitmaoActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_paint, R.string.view_paint_detail, SimplePaintActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_paint_game, R.string.view_paint_game_detail, GamePaintActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_anim, R.string.view_anim_detail, AnimActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_anim_bujian, R.string.view_anim_bujian_detail, BuJianAnimActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_anim_shuxing, R.string.view_anim_shuxing_detail, ShuXingAnimActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_tel, R.string.view_tel_detail, TelephoneyMainActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_vibrator, R.string.view_vibrator_detail, VibratorActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_sensor, R.string.view_sensor_detail, SensorActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_demo, R.string.view_demo_detail, DemoMainActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_demo_nd, R.string.view_demo_nd_detail, DemoMain2Activity.class));
        demoDetailses.add(new DemoDetails(R.string.view_demo_rd, R.string.view_demo_rd_detail, DemoMain3Activity.class));
        demoDetailses.add(new DemoDetails(R.string.view_recyclerview, R.string.view_recyclerview_detail, RecyclerViewActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_recyclerview, R.string.view_card_detail, CardViewActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_recyclerview, R.string.view_notification_detail, MyNotificationActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_retrofit, R.string.view_retrofit_detail, RetrifitMainActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_md, R.string.view_md_detail, MDMainactivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_md, R.string.view_md_tablayout, MDTabLayoutActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_md, R.string.view_md_coorlayout, MDCoorActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_md, R.string.view_md_coordetail, MDCoorDetailActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_view, R.string.view_view_detail, CustomMeasureActivity.class));
        demoDetailses.add(new DemoDetails(R.string.view_view, R.string.view_cusview_detail, SimpleCustomActivity.class));
//        demoDetailses.add(new DemoDetails(R.string.view_shareelement, R.string.view_shareelement_detail, SimpleCustomActivity.class));

        Log.i(TAG, "initData: " + demoDetailses.size());

        homeAdapter = new HomeAdapter(context, demoDetailses);
        homeList.setAdapter(homeAdapter);

    }


    public static ExecutorService newFixedThreadPool(int nThreads){

        ThreadFactory factory=new ThreadFactory() {
            @Override
            public Thread newThread(@NonNull Runnable r) {
                return newThread(new Runnable() {
                    @Override
                    public void run() {

                        LogUtil.d("-------------");

                    }
                });
            }
        };

        return new ThreadPoolExecutor(nThreads,
                nThreads,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());

    }

    @Override
    protected void initListener() {

        homeAdapter.setOnRecyclerViewClickListener(new HomeAdapter.OnRecyclerViewClick() {
            @Override
            public void RecyclerViewClick(int position) {
//                showShortToast(position + "");

                if (demoDetailses.get(position).getActivityClass() != null) {
                    openActivity(demoDetailses.get(position).getActivityClass());
                }

            }
        });

    }


}
